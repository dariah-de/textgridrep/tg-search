## [5.1.1](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-search/compare/v5.1.0...v5.1.1) (2024-12-06)


### Bug Fixes

* raise aggregation limit for portalconfig requests ([40c11f7](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-search/commit/40c11f7a913b8e0894b6b9748fe81a05263ed9c4))

# [5.1.0](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-search/compare/v5.0.0...v5.1.0) (2024-09-12)


### Features

* add portalhelper methods for project info and toplevel to interface ([8fd3022](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-search/commit/8fd3022a801037d300397dcade420554d1e73a14))

# [5.0.0](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-search/compare/v4.2.0...v5.0.0) (2024-05-23)


### Build System

* **maven:** update elasticsearch dependency to 7.9.3 ([bfb9bd3](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-search/commit/bfb9bd3ea37c688527563ef5d3411d291487c011))


### Reverts

* Revert "Revert "fix: track total hits for search result"" ([95e0bf3](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-search/commit/95e0bf39a8c47a0db5de6f99f9f26fb7c7f13bb8))
* Revert "build(maven): use httpclients 4.7.1-SNAPSHOT" ([f8b598a](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-search/commit/f8b598afa48590ced5e25ba074444c5526287c52))


### BREAKING CHANGES

* **maven:** requires elasticsearch server >= 7

# [4.2.0](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-search/compare/v4.1.0...v4.2.0) (2023-06-20)


### Bug Fixes

* **PortalHelperImpl:** projectFilter must never be null ([9993ff7](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-search/commit/9993ff724829968e1ac68840f42721cb39891f0c))


### Features

* endpoint to inspect config params. allow autowiring spring beans by annotation ([ef12d07](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-search/commit/ef12d07863a69888c421ea1b4cbc0f2646e63445))
* facetquery service for nonpublic data ([0e1b7c4](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-search/commit/0e1b7c4542f510f85177561a3306a2796fb7ad57))
* make INDEX_KEY_EXCEPTIONS configurable and rename to untouchedFieldExceptions ([e91484b](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-search/commit/e91484b962f84b345cf36adc04a2d8f3ce48d22e))
* **PortalHelper:** filter access on toplevel and project requests ([48d5d9d](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-search/commit/48d5d9dfda5f4c041f8238fde06d0142b26cff60))
* **PortalHelper:** list non public projects with access filter ([a50c3f0](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-search/commit/a50c3f03ba959e71580b122c9f8a03f8f149d25a))
* toplevelcache for nonpublic data shall expire faster ([ce0c37b](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-search/commit/ce0c37b327f83d1e1920672e84c3824ec8c09328))

# [4.1.0](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-search/compare/v4.0.6...v4.1.0) (2023-02-28)


### Bug Fixes

* **ci:** fix yaml typo ([126926a](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-search/commit/126926addbe649f769aa973ca3328ff2497873ed))


### Features

* add new GitLab CI workflow ([e0a6867](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-search/commit/e0a6867a52d708fadeef097ce21beb8cde2895b3))

# 4.0.3-SNAPSHOT

- Repository moved to Gitlab
