# TextGrid search (tgsearch)

## Public and Non-Public

To build service choose a profile, public or nonpublic. Public ist default, so you may create public package with `mvn package`.
Or start locally with `mvn tomcat:run`

To have a nonpublic-tgsearch copy src/main/webapp/WEB-INF/tgsearch.nonpublic.properties.tmpl to
src/main/webapp/WEB-INF/tgsearch.nonpublic.properties and edit it. Then use param `-P nonpublic` for `tomcat:run` or `package`.

Local instance started with `tomcat:run` will be at http://localhost:8080/tgsearch-service/ .
Deployed war should be at http://localhost:8080/tgsearch or http://localhost:8080/tgsearch-public .

Use `mvn jetty:run` to start in local jetty container.


## Release Workflow

The release workflow for DARIAH-DE services is documented in the DARIAH-DE wiki <https://projects.academiccloud.de/projects/dariah-de-intern/wiki/dariah-de-release-management>.

