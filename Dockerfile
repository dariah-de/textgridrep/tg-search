FROM maven:3.8.7-eclipse-temurin-17 as builder

COPY . /build
WORKDIR /build

RUN --mount=type=cache,target=/root/.m2 mvn clean package

FROM tomcat:10.1-jre17

ARG JOLOKIA_VERSION="1.3.7" # jolokia as of 1.5.0 requires auth for proxy operation

ENV JAVA_OPTS="-Xmx768m"
ENV CATALINA_OPTS="-Dcom.sun.management.jmxremote.port=9999 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"

COPY --from=builder /build/tgsearch-public-webapp/target/tgsearch-public.war /usr/local/tomcat/webapps/tgsearch-public.war
COPY --from=builder /build/tgsearch-nonpublic-webapp/target/tgsearch-nonpublic.war /usr/local/tomcat/webapps/tgsearch.war
RUN curl -XGET "https://repo1.maven.org/maven2/org/jolokia/jolokia-war/${JOLOKIA_VERSION}/jolokia-war-${JOLOKIA_VERSION}.war" --output /usr/local/tomcat/webapps/jolokia.war

COPY Dockerfile /

RUN groupadd --system --gid 1007 tomcat-tgsearch
RUN useradd --system --uid 1007 --gid 1007 tomcat-tgsearch
RUN chown -R tomcat-tgsearch:tomcat-tgsearch /usr/local/tomcat/webapps/

USER tomcat-tgsearch

WORKDIR /usr/local/tomcat/

EXPOSE 8080


