package info.textgrid.middleware.tgsearch;


import java.io.IOException;
import jakarta.xml.bind.JAXB;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.Strings;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import info.textgrid.middleware.tgsearch.auth.NoAuthClient;
import info.textgrid.middleware.tgsearch.auth.TGAuthClient;
import info.textgrid.namespaces.middleware.tgsearch.PathResponse;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.utils.sesameclient.SesameClient;



public class ElasticSearchTest {

	private static TGElasticSearchClient esClient;
	private static SesameClient sc;
	private static ElasticSearchNavigationImpl esni;
	private static ElasticSearchNavigationImpl esniAuth;
	private static ElasticSearchImpl esiNoAuth;
	private static PathBuilder pb;


	@BeforeClass
	public static void setUp() throws Exception {

		sc = new SesameClient("https://dev.textgridlab.org/triplestore/textgrid-public");

		esClient = new TGElasticSearchClient(sc, "localhost", new int[]{9202, 9203}, 2000);
		esClient.setEsIndex("textgrid-public");

		esiNoAuth = new ElasticSearchImpl(new NoAuthClient(), esClient);
		esni = new ElasticSearchNavigationImpl(new NoAuthClient(), sc, esClient);
		esniAuth = new ElasticSearchNavigationImpl(new TGAuthClient(null), sc, esClient);
		pb = new PathBuilder(esClient);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	@Ignore
	public void testSandbox() throws IOException{

    	SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    	searchSourceBuilder.fetchSource(
    	    new String[] {"textgridUri", "nearlyPublished"}, Strings.EMPTY_ARRAY
    	).query(
    	    QueryBuilders.boolQuery().mustNot(QueryBuilders.existsQuery("nearlyPublished"))
    	).size(1);

    	SearchRequest getRecordList = new SearchRequest(esClient.getEsIndex())
    			.source(searchSourceBuilder);

    	SearchResponse getRecordListItems = esClient.getEsClient().search(getRecordList, RequestOptions.DEFAULT);
    	System.out.println(getRecordListItems.getHits().getTotalHits());

    	System.out.println(getRecordList);

    	for(SearchHit hit : getRecordListItems.getHits().getHits()){
    		System.out.println(hit.getSourceAsMap().get("textgridUri").toString());
    		System.out.println(hit.getSourceAsMap().get("nearlyPublished").toString());
    	}
	}


	@Test
	@Ignore
	public void testElasticSearchImplNoAuth() throws IOException {

		Response r = esiNoAuth.getQuery("Goethe", "metadata", "", "", "","relevance",0,20,40,0,"","","", null, null,10);
		JAXB.marshal(r, System.out);
	}

	@Test
	@Ignore
	public void testElasticSearchImplNoAuth1() throws IOException {

		Response r = esiNoAuth.getQuery("Goethe", "structure", "", "", "","relevance",0,20,40,0,"","","", null, null,10);
		JAXB.marshal(r, System.out);
	}
	@Test
	@Ignore
	public void testElasticSearchImplNoAuth2() throws IOException {

		Response r = esiNoAuth.getQuery("Goethe", "structure", "", "", "","relevance",0,20,40,0,"","","", null, null,10);
		JAXB.marshal(r, System.out);
	}
	@Test
	@Ignore
	public void testElasticSearchImplNoAuth3() throws IOException {

		Response r = esiNoAuth.getQuery("Goethe", "structure", "", "", "","relevance",0,20,40,0,"","","", null, null,10);
		JAXB.marshal(r, System.out);
	}
	@Test
	@Ignore
	public void testElasticSearchImplNoAuth4() throws IOException {

		Response r = esiNoAuth.getQuery("Goethe", "structure", "", "", "","relevance",0,20,40,0,"","","", null, null,10);
		JAXB.marshal(r, System.out);
	}
	@Test
	@Ignore
	public void testElasticSearchImplNoAuth5() throws IOException {

		Response r = esiNoAuth.getQuery("Goethe", "structure", "", "", "","relevance",0,20,40,0,"","","", null, null,10);
		JAXB.marshal(r, System.out);
	}
	@Test
	@Ignore
	public void testElasticSearchImplNoAuth6() throws IOException {

		Response r = esiNoAuth.getQuery("Goethe", "structure", "", "", "","relevance",0,20,40,0,"","","", null, null,10);
		JAXB.marshal(r, System.out);
	}
	@Test
	@Ignore
	public void testElasticSearchImplNoAuth7() throws IOException {

		Response r = esiNoAuth.getQuery("Goethe", "structure", "", "", "","relevance",0,20,40,0,"","","", null, null,10);
		JAXB.marshal(r, System.out);
	}
	@Test
	@Ignore
	public void testElasticSearchNavigationImplNoAuthAggregation(){

		Response r = esni.listAggregation("textgrid:jfsm.0", "", "");

		JAXB.marshal(r, System.out);


		/*List<String> projectsAllowedToSearch = new ArrayList<String>();
		projectsAllowedToSearch.add("p1");
		projectsAllowedToSearch.add("p2");

		FilterBuilder projectFilter = FilterBuilders.boolFilter().must(FilterBuilders.termFilter("project.id",  projectsAllowedToSearch));*/

	}

	@Test
	@Ignore
	public void testElasticSearchNavigationImplAuthAggregation(){

	Response r = esniAuth.listAggregation("textgrid:jfsm.0", "", "");

		JAXB.marshal(r, System.out);


		/*List<String> projectsAllowedToSearch = new ArrayList<String>();
		projectsAllowedToSearch.add("p1");
		projectsAllowedToSearch.add("p2");

		FilterBuilder projectFilter = FilterBuilders.boolFilter().must(FilterBuilders.termFilter("project.id",  projectsAllowedToSearch));*/

	}

	@Test
	@Ignore
	public void testElasticSearchNavigationImplTopAggregation(){

		Response r = esni.listToplevelAggregations(false);

		JAXB.marshal(r, System.out);

	}

	@Test
	@Ignore
	public void testJustPath(){

		PathResponse resp = pb.listPath("textgrid:n2kn.0");
		JAXB.marshal(resp, System.out);
	}
}

