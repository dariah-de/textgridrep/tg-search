/*
 * #%L TextGrid :: TGSearch :: Core
 *
 * %% Copyright (C) 2011-2019 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen
 * (http://www.sub.uni-goettingen.de) %%
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L%
 */

package info.textgrid.middleware.tgsearch.utils;

import info.textgrid.middleware.tgsearch.TGSearchImplConstants;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.TextgridUri;
import info.textgrid.namespaces.metadata.core._2010.GenericType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;
import info.textgrid.utils.sesameclient.SesameClient;
import info.textgrid.utils.sesameclient.TGSparqlUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

public final class TGSearchUtils {

  private static final MustacheFactory mustacheFactory = new DefaultMustacheFactory();

  private TGSearchUtils() {
    // not called
  }

  private static Log log = LogFactory.getLog(TGSearchUtils.class);

  /**
   * Append metadata-objects only containing uri to response, if uri is in allUris and not in
   * allowedUris.
   *
   * @param response
   * @param allUris
   * @param allowedUris
   * @return response
   */
  public static Response addEmptyNodes(Response response, Collection<String> allUris,
      List<String> allowedUris) {

    allUris.removeAll(allowedUris);

    for (String uri : allUris) {
      ResultType result = createUriOnlyResult(uri);
      response.getResult().add(result);
    }

    return response;
  }

  /**
   * Create resultType with only the textgriduri set.
   *
   * @param uri
   * @return result
   */
  public static ResultType createUriOnlyResult(String uri) {

    TextgridUri tguri = new TextgridUri();
    tguri.setValue(uri);

    GeneratedType generated = new GeneratedType();
    generated.setTextgridUri(tguri);
    // revision is mandatory, take from uri
    generated.setRevision(revisionFromUri(uri));

    GenericType generic = new GenericType();
    generic.setGenerated(generated);

    ObjectType object = new ObjectType();
    object.setGeneric(generic);

    ResultType result = new ResultType();
    result.setObject(object);
    result.setAuthorized(false);

    return result;

  }

  /**
   * find all textgridUri elements in inputstream, return them as list
   *
   * @param in inputstream of xml
   * @return textgrid uris found
   */
  // not used?
  public static List<String> extractUris(InputStream in, XMLInputFactory xif) {

    List<String> uris = new ArrayList<String>();

    try {
      XMLStreamReader r = xif.createXMLStreamReader(in);
      while (r.hasNext()) {
        if ((r.next() == XMLStreamConstants.START_ELEMENT)
            && (r.getLocalName().equalsIgnoreCase("textgridUri"))) {
          // if (r.getLocalName().equalsIgnoreCase("textgridUri") ) {
          uris.add(r.getElementText());
          // }
        }
      }
      r.close();
    } catch (XMLStreamException e) {
      // e.printStackTrace();
      log.error("extractUris: could not read XML", e);
    }

    return uris;

  }

  // not used?
  public static List<String> extractUrisFromValues(InputStream in, XMLInputFactory xif) {
    List<String> uris = new ArrayList<String>();
    try {
      XMLStreamReader r = xif.createXMLStreamReader(in);
      while (r.hasNext()) {
        if ((r.next() == r.START_ELEMENT) && (r.getLocalName().equalsIgnoreCase("value")
            || r.getLocalName().equals("textgridUri"))) {
          // if (r.getLocalName().equalsIgnoreCase("value") ||
          // r.getLocalName().equals("textgridUri")) {
          uris.add(r.getElementText());
          // }
        }
      }
      r.close();
    } catch (XMLStreamException e) {
      // e.printStackTrace();
      log.error("extracrUrisFromValues: could not read XML", e);
    }
    return uris;

  }

  /**
   * get uris from oai-ore, optionally remove "textgrid:" uriprefix
   *
   * @param in oai-ore xml
   * @param xif xmlinputfactory
   * @param stripTgPrefix
   * @return uris
   * @throws XMLStreamException
   */
  public static List<String> extractUrisFromAggregation(InputStream in, XMLInputFactory xif, boolean stripTgPrefix)
      throws XMLStreamException {

    List<String> uris = new ArrayList<String>();

    QName resourceQN = new QName("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "resource");
    XMLEventReader reader = xif.createXMLEventReader(in);
    while (reader.hasNext()) {
      XMLEvent event = reader.nextEvent();
      if (event.getEventType() == XMLStreamConstants.START_ELEMENT &&
          event.asStartElement().getName().getLocalPart().equals("aggregates")) {

        String uri = event.asStartElement().getAttributeByName(resourceQN).getValue();
        if (stripTgPrefix) {
          uri = uri.substring(TGSearchImplConstants.TG_URI_PREFIX_LENGTH);
        }
        uris.add(uri);
      }
    }
    reader.close();
    return uris;

  }

  /**
   * split revsision from uri TODO: move to textgrid-utils
   *
   * @param uri
   * @return tglog
   */
  public static int revisionFromUri(String uri) {
    int revision = 0;
    String[] arr = uri.split("\\.");
    if (arr.length > 1) {
      revision = Integer.parseInt(arr[1]);
    }
    return revision;
  }

  /**
   * returns list of uris found in query, working for 1 bound variable, as every uri-xml-elements content is
   * returned in list. not namespace aware.
   *
   * sample for incoming xml:
   * <pre>{@code
   *    <sparql xmlns="http://www.w3.org/2005/sparql-results#">
   *    <head>
   *        <variable name="pre" />
   *    </head>
   *    <results>
   *        <result><binding name="pre"><uri>uri1</uri></binding></result>
   *    </results>
   *    </sparql>
   * }</pre>
   *
   * @param in inputstream from sparql-query
   * @return list of uris found
   */
  public static TreeSet<String> urisFromSparqlResponse(InputStream in, XMLInputFactory xif) {

    // a treeset which orders by textgrid-uris, shorter come first, then alphabetically
    TreeSet<String> urilist = new TreeSet<String>(new Comparator<String>() {
      @Override
      public int compare(String s1, String s2) {
        if(s1.length() < s2.length()) {
          return -1;
        }
        if(s1.length() > s2.length()) {
            return 1;
        }
        return s1.compareTo(s2);
      }
    });

    try {
      XMLStreamReader r = xif.createXMLStreamReader(in);
      while (r.hasNext()) {
        if ((r.next() == XMLStreamConstants.START_ELEMENT) && (r.getLocalName().equals("uri"))) {
          String uri = r.getElementText();
          urilist.add(uri);
        }
      }
      r.close();
    } catch (XMLStreamException e) {
      log.error("error parsing sparql-result-stream", e);
    }

    return urilist;
  }

  public static int countFromSparqlResponse(InputStream in, XMLInputFactory xif) {

    int count = -1; // -1 in error state

    try {
      XMLStreamReader r = xif.createXMLStreamReader(in);
      while (r.hasNext()) {
        if ((r.next() == XMLStreamConstants.START_ELEMENT) && (r.getLocalName().equals("literal"))) {
          count = Integer.parseInt(r.getElementText());
        }
      }
      r.close();
    } catch (XMLStreamException e) {
      log.error("error parsing sparql-result-stream", e);
    }

    return count;
  }

  public static String listToSequence(List<String> list) {
    StringBuffer sb = new StringBuffer();

    for (String item : list) {
      sb.append(item);
      sb.append(",");
    }

    if (sb.length() > 0) {
      sb.deleteCharAt(sb.length() - 1);
    }
    return sb.toString();
  }

  /**
   *
   * @param template
   * @param vars
   * @return writer
   */
  public static String createQueryFromTemplate(String template, HashMap<String, Object> vars) {
    Writer writer = new StringWriter();
    Mustache mustache = mustacheFactory.compile("/sparql-templates/"+template);
    mustache.execute(writer, vars);
    return writer.toString();
  }

  public static String latestRevision4baseUri(String uri, SesameClient sparqlClient) {

    // append textgrid: prefix if necessary
    if (!uri.startsWith(TGSearchImplConstants.TG_URI_PREFIX)) {
      uri = TGSearchImplConstants.TG_URI_PREFIX + uri;
    }

    try {
      uri = TGSparqlUtils.getLatestUri(sparqlClient, uri);
    } catch (IOException e) {
      log.debug("no revision uri found for " + uri);
    }
    return uri;
  }

}
