/*
 * #%L TextGrid :: TGSearch :: Core
 *
 * %% Copyright (C) 2011-2019 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen
 * (http://www.sub.uni-goettingen.de) %%
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L%
 */

package info.textgrid.middleware.tgsearch;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.action.search.SearchPhaseExecutionException;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import info.textgrid.middleware.tgsearch.api.Search;
import info.textgrid.middleware.tgsearch.api.TGSearchConstants;
import info.textgrid.middleware.tgsearch.auth.AuthClient;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgsearch.FacetGroupType;
import info.textgrid.namespaces.middleware.tgsearch.FacetResponseType;
import info.textgrid.namespaces.middleware.tgsearch.FacetType;
import info.textgrid.namespaces.middleware.tgsearch.FulltextType;
import info.textgrid.namespaces.middleware.tgsearch.FulltextType.Kwic;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;


/**
 * ElasticSearchImpl gives the basic functionalities to enable the search function. From this class
 * the the query will be generated with all necessary parameters. Also the authentication for the
 * non-public region will be handled by this class. Another function is the choice of the search
 * type in textgridrep
 *
 * @author Ubbo Veentjer
 * @author Maximilian Brodhun Version 1.2
 */

public class ElasticSearchImpl implements Search {

  private AuthClient authClient;
  private Log log = LogFactory.getLog(ElasticSearchImpl.class);
  private TGElasticSearchClient tgEsclient;
  private PathBuilder pathBuilder;

  private boolean filterProjects4Auth = true;

  public ElasticSearchImpl(AuthClient authClient, TGElasticSearchClient tgesClient) {
    this.authClient = authClient;
    this.tgEsclient = tgesClient;
    this.pathBuilder = new PathBuilder(tgesClient);
  }

  @Override
  @GET
  @Path("/")
  @Produces({"application/xml", "text/xml"})
  public Response getQuery(
      @QueryParam("q") String query,
      @QueryParam("target") @DefaultValue("both") String target,
      @QueryParam("sid") String sid,
      @QueryParam("log") String logString,
      @QueryParam("sessionid") @DefaultValue("") String sessionid,
      @QueryParam("order") @DefaultValue("relevance") String order,
      @QueryParam("start") @DefaultValue("0") int start,
      @QueryParam("limit") @DefaultValue("20") int limit,
      @QueryParam("kwicWidth") @DefaultValue("40") int kwicWidth,
      @QueryParam("wordDistance") @DefaultValue("-1") int wordDistance,
      @QueryParam("path") @DefaultValue("false") String path,
      @QueryParam("allProjects") @DefaultValue("false") String allProjects,
      @QueryParam("sandbox") @DefaultValue("false") String sandbox,
      @QueryParam("filter") List<String> filters,
      @QueryParam("facet") List<String> facetList,
      @QueryParam("facetlimit") @DefaultValue("10") int facetLimit) {

    Response rootNode = new Response();
    rootNode.setHits("0");
    // ---Preparation for search operation ---

    String queryTmp = query;

    /**
     * Some symbols are reserved for lucene syntax. If the query starts or ends if one these symbols
     * the query results in an server error
     */

    if (query != null && (query.endsWith("!") || (query.startsWith(":")) || query.endsWith(":"))) {
      return rootNode;
    }

    if(facetLimit == 0) {
      facetLimit = ElasticSearchFacetImpl.MAX_FACETS;
    }

    QueryBuilder projectFilter = null;
    boolean pathNeeded = Boolean.valueOf(path);
    boolean searchAllProjects = Boolean.parseBoolean(allProjects);

    // set if authclient and not searchallprojects
    try {

      if (!authClient.isProjectAuth()) {
        log.debug("no auth enabled");
        filterProjects4Auth = false;
      } else if (searchAllProjects) {
        log.debug("search all Projects requested");
        log.debug(order);

        // textgridlab now queries a limit of 2147483647, which kills elastic search client
        limit = TGSearchImplConstants.URI_LIMIT_AUTH;

        return queryAllProjects(query, sid, limit);

        // TODO: set to false when implemented
        // filterProjects4Auth = false;
      } else {
        filterProjects4Auth = true;
        log.debug("query auth for searchable projects");

        // get list of projects browseable by user
        List<String> projectsAllowedToSearch = authClient
            .getProjectsForSID(sid);

        projectFilter = QueryBuilders.termsQuery("project.id", projectsAllowedToSearch);

        // TODO: a message could be send to lab if list is empty,
        // no need for searching then

        if (projectsAllowedToSearch.isEmpty()) {
          log.debug("Auth enabled, but no searchable Projects found for this user");
          return rootNode;
        }

      }

      QueryBuilder queryMode = setQueryMode(target, queryTmp);

// TODO
      // boost hits in fulltext_structured.title
//      queryMode = new BoolQueryBuilder().must(queryMode)
//          .should(new MatchQueryBuilder("fulltext_structured.title", query).boost(2));

      /**
       * FILTERS
       *
       * TODO: - post-filter is slower than a filtered query, as the search is exectued first, and
       * filtered afterwards in the later case
       * http://elasticsearch-users.115913.n3.nabble.com/filtered-query-vs-query-w-filter-td4040423.html
       *
       * - use bool filter and no and filter for performance:
       * http://www.elasticsearch.org/blog/all-about-elasticsearch-filter-bitsets/
       *
       * - and set cache to true...?
       * http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/query-dsl-bool-filter.html
       */

      //BoolFilterBuilder queryfilter = FilterBuilders
      //    .boolFilter();

      BoolQueryBuilder queryfilter = QueryBuilders.boolQuery();

      // don't show hits from sandbox if not requested, makes only sense for public tgsearch
      if (!authClient.isProjectAuth() && !Boolean.parseBoolean(sandbox)) {
        queryfilter.mustNot(QueryBuilders.existsQuery("nearlyPublished"));
      }

      if (filterProjects4Auth) {
        queryfilter.must(projectFilter);
      }

      if (filters != null) {
        for (String filter : filters) {

          if (filter.contains(":")) {
            String[] filterArr = filter.split(":", 2);
            String filterField = filterArr[0];
            String filterTerm = filterArr[1];

            if (!filterField.endsWith(".untouched") &! this.tgEsclient.getUntouchedFieldExceptions().contains(filterField)) {
              filterField += ".untouched";
            }

            if (filter.startsWith("!")) {
              filterField = filterField.substring(1);
              queryfilter.mustNot(QueryBuilders.termQuery(filterField, filterTerm));
            } else {
              queryfilter.must(QueryBuilders.termQuery(filterField, filterTerm));
            }
          }
        }
      }

      /**
       * apply filters and query to final query
       */
      QueryBuilder finalQuery;
      if (queryfilter.hasClauses()) {
        if (query == null) {
          // no query, just filter
          finalQuery = QueryBuilders.boolQuery()
              .must(QueryBuilders.matchAllQuery())
              .filter(queryfilter);
        } else {
          // query with filter
          finalQuery = QueryBuilders.boolQuery()
              .must(queryMode)
              .filter(queryfilter);
        }
      } else {
        // no filter, just query
        finalQuery = queryMode;
      }


      /**
       * Building the search request by giving the information in which index type the query will be
       * done. The highlighted fields will be shown on textgridrep.de in red color with "addFields"
       * the fields which are loaded after the search request are limited to the given
       */
      SearchSourceBuilder mainTGSearchRequestSource = setSortOrder(order, start, limit, finalQuery, kwicWidth);

      for (String facet : facetList) {
        if (!facet.endsWith(".untouched") &! this.tgEsclient.getUntouchedFieldExceptions().contains(facet)) {
          facet += ".untouched";
        }
        AggregationBuilder agg = AggregationBuilders.terms(facet)
            .field(facet)
            .size(facetLimit);

        mainTGSearchRequestSource.aggregation(agg);
      }
      // --- Query Execution ---
      /**
       * Execution of the previous build query and print of the resulting amount of items
       */

      SearchRequest mainTGSearchRequest = new SearchRequest(this.tgEsclient.getEsIndex())
          .searchType(SearchType.QUERY_THEN_FETCH);
      mainTGSearchRequest.source(mainTGSearchRequestSource);

      //log.debug(mainTGSearchRequest.toString());

      SearchResponse esResponse = null;
      try {
        esResponse = this.tgEsclient.getEsClient()
            .search(mainTGSearchRequest, RequestOptions.DEFAULT);
      } catch (SearchPhaseExecutionException e) {
        // TODO: error should be communicated to client
        if (e.getMessage().contains("SearchParseException")) {
          log.info("could not parse query: " + query);
        } else {
          log.error(e.getMessage());
        }
        return rootNode;
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

      SearchHits matchingHits = esResponse.getHits();

      /**
       * Preparation to print the result in XML format by using JAXBContext and an unmarshaller
       */
      Unmarshaller unmarshaller = this.tgEsclient.jaxbContext().createUnmarshaller();

      /**
       * For each iteration to decode the original metadata set in the "original_metadata" field.
       * Previously it was decoded in base64
       */

      for (SearchHit singleSearchHit : matchingHits.getHits()) {

        ResultType fetchedMetadataSets = new ResultType();

        String originalMetadataSet = singleSearchHit.getSourceAsMap()
            .get(TGSearchImplConstants.ES_ORIGINAL_METADATA_FIELD)
            .toString();

        ObjectType metadataSet = this.tgEsclient.metdataJaxbFromString(unmarshaller, originalMetadataSet);
        fetchedMetadataSets.setObject(metadataSet);

        if (pathNeeded) {
          fetchedMetadataSets.setPathResponse(pathBuilder.listPath(singleSearchHit.getSourceAsMap()
              .get("textgridUri").toString()));

        }

        /* KWIC matches */
        if (!singleSearchHit.getHighlightFields().isEmpty()) {
          FulltextType fulltextDocumentCorrespondingToMetadata = new FulltextType();
          if (singleSearchHit.getHighlightFields()
              .get(TGSearchImplConstants.ES_FULLTEXT_FIELD) != null) {
            for (Text hf : singleSearchHit.getHighlightFields()
                .get(TGSearchImplConstants.ES_FULLTEXT_FIELD)
                .getFragments()) {
              Kwic kw = buildKwic(hf.toString(), null, null);
              fulltextDocumentCorrespondingToMetadata.getKwic().add(kw);
            }
          }
          fetchedMetadataSets.getFulltext().add(fulltextDocumentCorrespondingToMetadata);
        }

        /* get author from edition, if available */
        if (singleSearchHit.hasSource()) {
          Map<String, Object> source = singleSearchHit.getSourceAsMap();
          if (source.containsKey("edition")) {

            HashMap edition_source = (HashMap) source.get("edition");
            Object agent_source = edition_source.get("agent");

            if (agent_source.getClass().equals(java.util.ArrayList.class)) {
              for (HashMap agent_source_hm : ((ArrayList<HashMap<String, Object>>) agent_source)) {
                if (agent_source_hm.get("role").equals("author")) {
                  fetchedMetadataSets.setAuthor(agent_source_hm.get("value").toString());
                }
              }
            } else if (agent_source.getClass().equals(java.util.HashMap.class)) {
              HashMap agent_source_hm = (HashMap) agent_source;
              if (agent_source_hm.get("role").equals("author")) {
                fetchedMetadataSets.setAuthor(agent_source_hm.get("value").toString());
              }
            }
          }
        }

        rootNode.getResult().add(fetchedMetadataSets);
      }

      /* add facets to response */
      if (esResponse.getAggregations() != null) {
        FacetResponseType facetResponse = new FacetResponseType();
        for (Entry<String, Aggregation> entry : esResponse.getAggregations().asMap().entrySet()) {
          FacetGroupType facetGroup = new FacetGroupType();
          String name = entry.getKey();
          if (name.endsWith(".untouched")) {
            name = name.substring(0, name.lastIndexOf("."));
          }
          facetGroup.setName(name);

          Terms a = (Terms) entry.getValue();
          for (Bucket bentry : a.getBuckets()) {
            FacetType facet = new FacetType();
            facet.setCount(bentry.getDocCount());
            facet.setValue(bentry.getKey().toString());
            facetGroup.getFacet().add(facet);
          }
          facetResponse.getFacetGroup().add(facetGroup);
        }
        rootNode.setFacetResponse(facetResponse);
      }

      rootNode.setHits("" + matchingHits.getTotalHits().value);
    } catch (JAXBException e) {
      log.error(e);
    } catch (XMLStreamException e) {
      log.error(e);
    } catch (FactoryConfigurationError e) {
      log.error(e);
    }

    rootNode.setLimit("" + limit);
    rootNode.setStart("" + start);
    rootNode.setSession("deprecated");

    return rootNode;
  }

  /**
   * run a query over all data and filter found uris by tgauth, mostly for usage with textgridlab,
   * to find public dataobjects like xsd and xslt
   *
   * would get very slow if bigger list of found uris needs to be authenticated against tgauth
   *
   * @param query
   * @param sid
   * @param limit
   * @return response
   */
  private Response queryAllProjects(String queryTmp, String sid, int limit) {

    log.debug("queryAllProjects for: " + queryTmp);

    QueryStringQueryBuilder esQuery = QueryBuilders.queryStringQuery(queryTmp);

    SearchRequest getAllUris = new SearchRequest(
        this.tgEsclient.getEsIndex(),
        this.tgEsclient.getEsPublicIndex())
        .searchType(SearchType.QUERY_THEN_FETCH);
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

    // set fields
    String[] includeFields = new String[] {
        TGSearchImplConstants.TG_UNIQUE_IDENTIFIER_FIELD,
        TGSearchImplConstants.ES_ORIGINAL_METADATA_FIELD
    };
    String[] excludeFields = Strings.EMPTY_ARRAY;

    searchSourceBuilder
      .fetchSource(includeFields, excludeFields)
      .size(limit)
      .query(esQuery);

    getAllUris.source(searchSourceBuilder);

    log.debug(getAllUris);

    SearchResponse esResponse;
    ArrayList<String> allUris = new ArrayList<String>();
    try {
      esResponse = tgEsclient.getEsClient().search(getAllUris, RequestOptions.DEFAULT);
      for (SearchHit singleSearchHit : esResponse.getHits().getHits()) {
        allUris.add(singleSearchHit.getSourceAsMap().get("textgridUri").toString());
      }
    } catch (IOException e) {
      log.error(e);
    }

    List<String> allowedUris = authClient.filterUris(sid, allUris);

    log.debug("uris found: " + allUris.size() + " - allowed: " + allowedUris.size());

    Response response = new Response();
    response = this.tgEsclient.metaData4Uris(allowedUris, response, false, true);

    return response;

  }


  /**
   *
   * @param order How the results will be sorted
   * @param mainTGSearchRequest search syntax for ElasticSearch
   * @param start result number to start with
   * @param limit number of entries to return
   * @param queryMode ElasticSearch syntax for performing the search on specific fields
   */

  private SearchSourceBuilder setSortOrder(String order, int start, int limit,
      QueryBuilder queryMode, int kwicWidth) {

    /**
     * In cases that the search results should be presented in another way as relevance:
     *
     * Sort order must be modified since the whole string consists of asc:format or desc:format but
     * ElasticSearch requires only to have "asc" or "desc" in the string
     *
     */

    String[] completeSortOrderString;
    String esSortOrderForTGRep = "";
    String esSortByTGField = "";

    if (!order.equals("relevance")) {
      completeSortOrderString = order.split(":");

      esSortOrderForTGRep = completeSortOrderString[0];
      esSortByTGField = completeSortOrderString[1];

      if (!esSortByTGField.endsWith(".untouched")) {
        esSortByTGField += ".untouched";
      }

    }

    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

    // set fields
    String[] includeFields = new String[] {
        TGSearchImplConstants.TG_UNIQUE_IDENTIFIER_FIELD,
        TGSearchImplConstants.ES_ORIGINAL_METADATA_FIELD,
        "edition.agent.*"
    };
    String[] excludeFields = Strings.EMPTY_ARRAY;
    searchSourceBuilder.fetchSource(includeFields, excludeFields);

    // set highlighter
    HighlightBuilder highlightBuilder = new HighlightBuilder();
    HighlightBuilder.Field highlightFulltext =
        new HighlightBuilder.Field("fulltext").fragmentSize(kwicWidth);
    highlightBuilder.field(highlightFulltext);
    //System.out.println("disabling experimantal highlighter for now");
    //highlightBuilder.field(buildStructuredFulltextHighlighter());
    //highlightBuilder.field(buildStructuredFulltextTitleHighlighter());
    searchSourceBuilder.highlighter(highlightBuilder);

    // set order
    if (!order.equals(TGSearchConstants.DEFAULT_SORT_ORDER)
        && (esSortOrderForTGRep.equals("asc"))) {
      searchSourceBuilder.sort(new FieldSortBuilder(esSortByTGField).order(SortOrder.ASC));
    } else if (!order.equals(TGSearchConstants.DEFAULT_SORT_ORDER)
        && (esSortOrderForTGRep.equals("desc"))) {
      searchSourceBuilder.sort(new FieldSortBuilder(esSortByTGField).order(SortOrder.DESC));
    }

    searchSourceBuilder.query(queryMode).from(start).size(limit).trackTotalHits(true);

    return searchSourceBuilder;
  }

  /**
   *
   * @param target where to do the search operation: fulltext, metadata or both
   * @param query the query string for ElasticSearch
   * @param queryMode ElasticSearch syntax for performing the search on specific fields
   */

  private QueryBuilder setQueryMode(String target, String query) {

    QueryBuilder queryModeTmp = null;


    if (target.equals(TGSearchConstants.BOTH)) {
      queryModeTmp = QueryBuilders.queryStringQuery(query).defaultOperator(Operator.AND);
    }


    else if (target.equals(TGSearchConstants.FULLTEXT)) {

      if (query.startsWith("\"") && query.endsWith("\"")) {
        queryModeTmp = QueryBuilders.matchPhraseQuery(TGSearchImplConstants.ES_FULLTEXT_FIELD, query).slop(0);
      } else {
        queryModeTmp = QueryBuilders.queryStringQuery(query)
            .defaultField(TGSearchImplConstants.ES_FULLTEXT_FIELD).defaultOperator(Operator.AND);
      }
    } else if (target.equals(TGSearchConstants.METADATA)) {
      queryModeTmp = QueryBuilders.queryStringQuery(query)
          .defaultField(TGSearchImplConstants.ES_ORIGINAL_METADATA_FIELD)
          .defaultOperator(Operator.AND);

    }

    return queryModeTmp;
  }


  /**
   * build the higlighter for the structured fulltext , which has id, title and content, using the
   * wikimedia experimental highlighter (https://github.com/wikimedia/search-highlighter/)
   *
   * @return
   */
  private HighlightBuilder.Field buildStructuredFulltextHighlighter() {
    HashMap<String, Object> options = new HashMap<String, Object>();
    String[] fetchFields = {"fulltext_structured.id", "fulltext_structured.title"};
    options.put("fetch_fields", fetchFields);

    return new HighlightBuilder
        .Field("fulltext_structured.fulltext")
        .highlighterType("experimental")
        .options(options);
  }

  private HighlightBuilder.Field buildStructuredFulltextTitleHighlighter() {
    HashMap<String, Object> options = new HashMap<String, Object>();
    String[] fetchFields = {"fulltext_structured.id"};
    options.put("fetch_fields", fetchFields);

    return new HighlightBuilder
        .Field("fulltext_structured.title")
        .highlighterType("experimental")
        .options(options);
  }

  /**
   * build a kwic jaxb object
   *
   * @param fragments a string containing the highlighted element with <em> tags
   * @param refId the id, if null the element is not set
   * @param refTitle the title, null if not there
   * @return
   */
  private Kwic buildKwic(String fragments, String refId, String refTitle) {
    Kwic kw = new Kwic();
    String[] sarray = TGElasticSearchClient.highlightString(fragments);
    kw.setLeft(sarray[0]);
    kw.setMatch(sarray[1]);
    kw.setRight(sarray[2]);

    kw.setRefId(refId);
    kw.setRefTitle(refTitle);

    return kw;
  }
}
