/*
 * #%L TextGrid :: TGSearch :: Core
 *
 * %% Copyright (C) 2011-2019 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen
 * (http://www.sub.uni-goettingen.de) %%
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L%
 */

package info.textgrid.middleware.tgsearch;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import info.textgrid.middleware.tgsearch.api.Navigation;
import info.textgrid.middleware.tgsearch.auth.AuthClient;
import info.textgrid.middleware.tgsearch.utils.TGSearchUtils;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.utils.sesameclient.SesameClient;


/**
 * This class implements the navigation functionality for ElasticSearch in TG-Search
 *
 * @author Maximilian Brodhun
 * @author Ubbo Veentjer
 * @version 1.2
 *
 */

public class ElasticSearchNavigationImpl implements Navigation {

  /**
   * Class parameters
   *
   * @param Log A log-file the navigation functionality in ElasticSearch
   * @param AuthClient contains the client for authentication
   * @param SesameClient Contains the sparqlClient
   *
   */

  private Log log = LogFactory.getLog(ElasticSearchNavigationImpl.class);
  private AuthClient authClient;
  private TGElasticSearchClient tgEsClient;
  private SesameClient sparqlClient;
  private XMLInputFactory xmlInputFactory;

  /**
   * The constructor for ElasticSearchNavigationImpl create an object with authClient and a
   * sparqlClient
   */

  public ElasticSearchNavigationImpl(AuthClient authClient,
      SesameClient sparqlClient, TGElasticSearchClient tgEsClient) {

    this.authClient = authClient;
    setSparqlClient(sparqlClient);
    setTgEsClient(tgEsClient);
    this.xmlInputFactory = XMLInputFactory.newInstance();
  }

  public TGElasticSearchClient getESClient() {

    return getClient();
  }

  /*
   * (non-Javadoc)
   *
   * @see info.textgrid.middleware.tgsearch.Navigation#listProject(java.lang.String )
   */
  @Override
  @GET
  @Path("/{id}")
  @Produces({"application/xml", "text/xml"})
  public Response listProject(@PathParam("id") String id,
      @QueryParam("sid") String sid, @QueryParam("log") String logstring) {

    HashMap<String, Object> tmplOptions = new HashMap<String, Object>();
    tmplOptions.put("projectId", id);
    String query = TGSearchUtils.createQueryFromTemplate("listProjectRoot.tmpl", tmplOptions);

    TreeSet<String> allUris = this.tgEsClient.getUrisFromQuery(query);

    log.debug("projectroot contains " + allUris.size() + " uris");
    List<String> allowedUris = authClient.filterUris(sid, allUris);

    Response response = new Response();
    response = this.tgEsClient.metaData4Uris(allowedUris, response);

    // With to many nodes the execution of the addEmptyNodes function kills the server
    if (!(allUris.size() > TGElasticSearchClient.getItemLimit())) {
      response = TGSearchUtils.addEmptyNodes(response, allUris, allowedUris);
    }

    return response;

  }



  /**
   * List content of aggregation with specific id
   *
   * @see info.textgrid.middleware.tgsearch.api.Navigation#listAggregation(java.lang.String,
   * java.lang.String, java.lang.String)
   *
   * @param id Identification of a project
   * @param sid Identification of a session
   * @param logstring
   * @return returns a list with Uris achieving the requirements of the sparql query
   */

  @Override
  @GET
  @Path("/agg/{id}")
  @Produces({"application/xml", "text/xml"})
  public Response listAggregation(
      @PathParam("id") String id,
      @QueryParam("sid") String sid,
      @QueryParam("log") String logstring) {

    /**
     * TODO: more robustness on data inconsistencies between original data and sesame retrieved data
     */

    // if baseuri lookup latest revision
    if (!id.contains(".")) {
      id = TGSearchUtils.latestRevision4baseUri(id, sparqlClient);
    }

    String idTmp = id;
    // Delete the prefix "textgrid:" from all TextgridUris
    if (id.startsWith("textgrid:")) {
      idTmp = id.substring(TGSearchImplConstants.TG_URI_PREFIX_LENGTH);
    }

    /**
     * retrieve original uris from ore / aggregation file
     */

    List<String> originalOrderedUris = new ArrayList<String>();
    try {

      String originalAggregation = this.tgEsClient.getOriginalDataObject(idTmp);
      log.debug("Second step get the original aggregation: " + originalAggregation);
      originalOrderedUris =
          TGSearchUtils.extractUrisFromAggregation(new ByteArrayInputStream(originalAggregation.getBytes()), this.xmlInputFactory, false);
      // log.info("Third step get original ordered uris. First element is: " +
      // originalOrderedUris.get(0));
    } catch (XMLStreamException e) {
      log.error("error reading agg: " + idTmp, e);
    } catch (FactoryConfigurationError e) {
      log.error("error reading agg: " + idTmp, e);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    /**
     * retrieve aggregation from sesame, lookup latest revision uris for baseuris
     */

    String queryTemplate = "listAggregation_nonpublic.tmpl";

    /*
     * if(this.authClient.isProjectAuth()) { queryTemplate = "listAggregation_nonpublic.tmpl";
     * log.info("Fourth step getting the queryTemplate: " + queryTemplate); } else { queryTemplate =
     * "listAggregation_public.tmpl"; log.info("Fourth step getting the queryTemplate: " +
     * queryTemplate); }
     */

    HashMap<String, Object> vars = new HashMap<String, Object>();
    vars.put("id", idTmp);
    log.debug("putting the initially given uri into a hashMap" + idTmp);
    String query = TGSearchUtils.createQueryFromTemplate(queryTemplate, vars);

    log.debug("Sixth step loading the SPARQL QUERY: " + query);

    TreeSet<String> revisionUris;
    if(this.authClient.isProjectAuth()) {
      // query both
      revisionUris = this.tgEsClient.getUrisFromQuery(query, true);
    } else {
      revisionUris = this.tgEsClient.getUrisFromQuery(query);
    }

    /**
     * create a map of revision uris, with baseuri as key
     */

    HashMap<String, String> revisionUriBaseMap = new HashMap<String, String>();
    for (String uri : revisionUris) {
      revisionUriBaseMap.put(uri.substring(0, uri.indexOf(".")), uri);
    }


    /**
     * create uriMap in original Order, with key beeing uri to retreive metadata for from elastic
     * search, and the value the name of the original uri
     */

    LinkedHashMap<String, String> orderedUriMap = new LinkedHashMap<String, String>();
    for (String originalUri : originalOrderedUris) {

      String uriForRevisionUriBaseMapLookup;

      // original uris may be baseuris or revision uris
      if (originalUri.indexOf(".") <= 0) {
        uriForRevisionUriBaseMapLookup = originalUri;
      } else {
        uriForRevisionUriBaseMapLookup = originalUri.substring(0, originalUri.indexOf("."));
      }
      String revisionUri = revisionUriBaseMap.get(uriForRevisionUriBaseMapLookup);
      // object may be deleted already, but still listed in agg file, so ignore entries with value
      // null (should have the deleted flag in sesame, which is filtered already)
      if (revisionUri != null) {
        orderedUriMap.put(revisionUri, originalUri);
      } else {
        log.debug("original agg " + idTmp + " contains deleted uri: " + originalUri);
      }

    }

    List<String> allowedUris = authClient.filterUris(sid, revisionUris);

    Response response = new Response();
    if(this.authClient.isProjectAuth()) {
      response = this.tgEsClient.metaData4UrisOriginalOrdered(orderedUriMap, response, allowedUris, true);
    } else {
      response = this.tgEsClient.metaData4UrisOriginalOrdered(orderedUriMap, response, allowedUris);
    }

    return response;

  }

  /**
   * List all toplevel objects, use for listing public repository from tglab, returns empty result
   * on nonpublic instances
   *
   * @param sandbox whether to include sandbox in listing, false by default
   * @return returns a list with all Uris in the TopLevel
   */
  @Override
  @GET
  @Path("/toplevel")
  @Produces({"application/xml", "text/xml"})
  public Response listToplevelAggregations(
      @QueryParam("sandbox") @DefaultValue("false") final boolean sandbox) {

    // only for public data
    if (!authClient.getClass().getSimpleName().equals("NoAuthClient")) {
      log.debug("non public instance, returning emtpy node");
      return new Response();
    }

    log.debug("generating new toplevel elements");

    Response response = new Response();

    HashMap<String, Object> tmplOptions = new HashMap<String, Object>();
    if (!sandbox) {
      tmplOptions.put("sandboxRestriction", "FILTER NOT EXISTS { ?s tg:isNearlyPublished ?inp } .");
    }
    String query = TGSearchUtils.createQueryFromTemplate("listTopLevel_public.tmpl", tmplOptions);

    log.debug("toplevelquery: \n" + query);

    TreeSet<String> uris = this.tgEsClient.getUrisFromQuery(query);
    response = this.tgEsClient.metaData4Uris(uris, response);

    return response;
  }

  public SesameClient getSparqlClient() {
    return sparqlClient;
  }

  public final void setSparqlClient(SesameClient sparqlClient) {
    this.sparqlClient = sparqlClient;
  }

  public TGElasticSearchClient getClient() {
    return tgEsClient;
  }

  public final void setTgEsClient(TGElasticSearchClient client) {
    this.tgEsClient = client;
  }

}
