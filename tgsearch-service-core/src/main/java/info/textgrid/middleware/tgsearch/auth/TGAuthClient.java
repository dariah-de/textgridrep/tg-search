/*
 * #%L TextGrid :: TGSearch :: Core
 *
 * %% Copyright (C) 2011-2019 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen
 * (http://www.sub.uni-goettingen.de) %%
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L%
 */

package info.textgrid.middleware.tgsearch.auth;

import info.textgrid.namespaces.middleware.tgauth.AuthenticationFault;
import info.textgrid.namespaces.middleware.tgauth.FilterBySidRequest;
import info.textgrid.namespaces.middleware.tgauth.FilterResponse;
import info.textgrid.namespaces.middleware.tgauth.GetObjectsRequest;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;
import info.textgrid.namespaces.middleware.tgauth.ResourcesetResponse;
import info.textgrid.namespaces.middleware.tgauth.RolesetResponse;
import info.textgrid.namespaces.middleware.tgauth.TgAssignedProjectsRequest;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TGAuthClient implements AuthClient {

  static final boolean PROJECT_AUTH = true;

  private PortTgextra tgp;
  private Log log = LogFactory.getLog(TGAuthClient.class);


  public TGAuthClient(PortTgextra tgExtraClient) {
    tgp = tgExtraClient;
  }

  /**
   *
   */
  @Override
  public List<String> filterUris(String sessionId, Collection<String> uris) {

    // do not modify incoming list, copy for retainAll operation
    List<String> response = new ArrayList<String>(uris);

    FilterBySidRequest filterBySidInput = new FilterBySidRequest();
    filterBySidInput.setAuth(sessionId);
    filterBySidInput.setOperation("read");
    // TODO: set logstring
    filterBySidInput.setLog("");
    filterBySidInput.getResource().addAll(response);

    List<String> allowed = null;
    try {
      log.debug("start filterUris: " + new Date());
      FilterResponse res = tgp.filterBySid(filterBySidInput);
      allowed = res.getResource();
      log.debug("end filterUris: " + new Date());
    } catch (AuthenticationFault e) {
      // log.error(e);
      log.error("filterUris: Failure regarding to sid ", e);
    }

    // we need to keep the order of the list, tgauth doesn't guarantee this, so intersect
    response.retainAll(allowed);
    return response;

  }

  @Override
  public List<String> getUrisForProject(String sessionId, String projectId) {

    GetObjectsRequest getObjectsInput = new GetObjectsRequest();
    getObjectsInput.setAuth(sessionId);
    // getObjectsInput.setLog(tglog.getLoginfo());
    getObjectsInput.setProject(projectId);

    log.debug("start getObjects: " + new Date());
    ResourcesetResponse res = tgp.getObjects(getObjectsInput);
    log.debug("end getObjects: " + new Date());
    List<String> uris = new ArrayList<String>(res.getResource());

    return uris;
  }

  @Override
  public List<String> getProjectsForSID(String sid) {

    TgAssignedProjectsRequest tgAssignedProjectsInput = new TgAssignedProjectsRequest();
    tgAssignedProjectsInput.setAuth(sid);
    RolesetResponse res = tgp.tgAssignedProjects(tgAssignedProjectsInput);

    return res.getRole();

  }

  @Override
  public boolean isProjectAuth() {
    // TODO Auto-generated method stub
    return TGAuthClient.PROJECT_AUTH;
  }

}
