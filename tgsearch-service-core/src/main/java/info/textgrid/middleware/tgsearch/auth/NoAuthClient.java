/*
 * #%L TextGrid :: TGSearch :: Core
 *
 * %% Copyright (C) 2011-2019 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen
 * (http://www.sub.uni-goettingen.de) %%
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L%
 */

package info.textgrid.middleware.tgsearch.auth;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class NoAuthClient implements AuthClient {



  static final boolean PROJECT_AUTH = false;

  @Override
  public List<String> filterUris(String sessionId, Collection<String> uris) {
    return new ArrayList<String>(uris);
  }

  @Override
  public List<String> getUrisForProject(String sid, String id) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<String> getProjectsForSID(String sid) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public boolean isProjectAuth() {
    // TODO Auto-generated method stub
    return NoAuthClient.PROJECT_AUTH;
  }

}
