/*
 * #%L TextGrid :: TGSearch :: Core
 *
 * %% Copyright (C) 2011-2019 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen
 * (http://www.sub.uni-goettingen.de) %%
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L%
 */

package info.textgrid.middleware.tgsearch.auth;

import java.util.Collection;
import java.util.List;

public interface AuthClient {

  List<String> filterUris(String sessionId, Collection<String> uris);

  List<String> getUrisForProject(String sid, String id);

  List<String> getProjectsForSID(String sid);

  /***
   * Whether this authClient does really authenticate against tgauth with a sid
   * (the TGAuthClient) or if it does just nothing (the NoAuthClient) can be asked
   * with this method. True for tgauth, false for noauth
   *
   * @return true if tgauth is queried for access rights
   */
  boolean isProjectAuth();

}
