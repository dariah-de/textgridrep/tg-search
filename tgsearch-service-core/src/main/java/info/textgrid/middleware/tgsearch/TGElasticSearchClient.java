/*
 * #%L TextGrid :: TGSearch :: Core
 *
 * %% Copyright (C) 2011-2019 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen
 * (http://www.sub.uni-goettingen.de) %%
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L%
 */

package info.textgrid.middleware.tgsearch;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpHost;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.elasticsearch.action.get.MultiGetRequest;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.Strings;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import info.textgrid.middleware.tgsearch.utils.TGSearchUtils;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.metadata.portalconfig._2020_06_16.Portalconfig;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;
import info.textgrid.utils.sesameclient.SesameClient;


/**
 * This class contains functions regarding to the ElasticSearch Client. This comprised the
 * establishing of connection in the constructor, to make sure that just one connection is
 * established and not for each search request. But also functions like the collections of the uris
 * for a specific query and the corresponding metadata nfor each uri.
 *
 * @author Maximilian Brodhun SUB-Göttingen
 * @author Ubbo Veentjer SUB-Göttingen
 *
 *         version 1.0
 */

public class TGElasticSearchClient {

  private Log log = LogFactory.getLog(TGElasticSearchClient.class);
  private static RestHighLevelClient esClient;
  private static String url;
  private static int[] ports;
  private static int itemLimit;
  private SesameClient sparqlClient;           // the sparqlclient for the tgsearch-public or nonpublic triplestore (set by config)
  private SesameClient federatedSparqlClient;  // a sparqlclient for federated queries in both triplestores (useful for tgsearch-nonpublic)
  private PathBuilder path4TGElement;
  private String esIndex;
  private String esPublicIndex; // this is only useful for tgsearch-nonpublic, for searchAllProjects
  private XMLInputFactory xmlInputFactory;
  private JAXBContext jaxbContext;

  // normally we have ".untouched" fields everywhere, but there are exceptions unfortunately :-/
  // we define or configure them here until the next mapping rework
  private List<String> untouchedFieldExceptions = Arrays.asList(new String[] {"project.id"});

  /**
   * Constructor
   *
   * @param sparqlClient address to the server for the sparql queries
   * @param url address to localhost
   * @param ports the ports for the elasticsearch client
   * @param itemLimit
   * @throws JAXBException
   */

  public TGElasticSearchClient(SesameClient sparqlClient, String url, int[] ports, int itemLimit) throws JAXBException {

    log.info("setting elasticsearchclient with url: " + url + " ports: " + ports);

    TGElasticSearchClient.setUrl(url);
    TGElasticSearchClient.setPorts(ports);
    TGElasticSearchClient.setItemLimit(itemLimit);

    List<HttpHost> hosts = new ArrayList<HttpHost>();
    for(int port : ports) {
      hosts.add(new HttpHost(url, port, "http"));
    }

    esClient = new RestHighLevelClient(
        RestClient.builder(
            hosts.toArray(new HttpHost[hosts.size()])));

    path4TGElement = new PathBuilder(this);
    this.sparqlClient = sparqlClient;
    // reuse that xmlInputFactory
    // reuse that! http://www.cowtowncoder.com/blog/archives/2006/06/entry_2.html
    this.xmlInputFactory = XMLInputFactory.newInstance();
    // reuse jaxbContext
    // https://javaee.github.io/jaxb-v2/doc/user-guide/ch03.html#other-miscellaneous-topics-performance-and-thread-safety
    this.jaxbContext = JAXBContext.newInstance(MetadataContainerType.class, Portalconfig.class);
  }

  /**
   * A String List containing the Uris from a specific query
   *
   * @param query The spaql query in String format
   * @return String List containing the uris generated by the response of the sparql query
   */
  public TreeSet<String> getUrisFromQuery(String query) {
    return getUrisFromQuery(query, false);
  }

  /**
   * A String List containing the Uris from a specific query
   *
   * @param query The spaql query in String format
   * @param useFederatedStore - wether to query a federation of public and nonpublic index
   * @return String List containing the uris generated by the response of the sparql query
   */
  public TreeSet<String> getUrisFromQuery(String query, boolean useFederatedStore) {
    InputStream sparqlResponse = null;
    try {
      if(useFederatedStore) {
        sparqlResponse = this.federatedSparqlClient.sparql(query);
      } else {
        sparqlResponse = sparqlClient.sparql(query);
      }
    } catch (IOException e) {
      log.error("getUrisFromQuery: could not read SPARQL query", e);
    }
    return TGSearchUtils.urisFromSparqlResponse(sparqlResponse, this.xmlInputFactory);
  }

  public int getCountFromQuery(String query) {
    InputStream sparqlResponse = null;
    try {
      sparqlResponse = sparqlClient.sparql(query);
    } catch (IOException e) {
      // log.error(e);
      log.error("getUrisFromQuery: could not read SPARQL query", e);
    }
    return TGSearchUtils.countFromSparqlResponse(sparqlResponse, this.xmlInputFactory);
  }


  /**
   * Takes the sparql query and sending it to the sparql client
   *
   * @param fragment A string containing the sparql query
   * @return A list with Uris
   */

  public static String[] highlightString(String fragment) {

    String[] stringToHighlight = new String[TGSearchImplConstants.STRING_TO_HIGHLIGHT_SIZE];
    int stringToHighlightStartElement = fragment.indexOf("<em>");
    int stringToHighlightEndElement = fragment.indexOf("</em>");

    stringToHighlight[0] = fragment.substring(0, stringToHighlightStartElement);
    stringToHighlight[1] = fragment.substring(stringToHighlightStartElement
        + TGSearchImplConstants.STRING_TO_HIGHLIGHT_STATEMENT_LENGTH_OPEN,
        stringToHighlightEndElement);
    stringToHighlight[2] = fragment.substring(stringToHighlightEndElement
        + TGSearchImplConstants.STRING_TO_HIGHLIGHT_STATEMENT_LENGTH_CLOSE);

    return stringToHighlight;

  }

  /**
   * Two versions of the metaData4Uris function two decide if the pathNeeded parameter is needed or
   * not
   */

  /**
   * Generating a list with all metadata for each Uri. Thereby using the multiGet function of
   * ElasticSearch. Therefore binding the XML documents from TextGrid to Java classes using JAXB
   *
   * @param uris unique identifier for the textgrid documents
   * @param response
   * @return returns the response containing the metadata set
   */
  public Response metaData4Uris(Collection<String> uris, Response response) {

    return metaData4Uris(uris, response, false);
  }

  /**
   * Generating a list with all metadata for each Uri. Thereby using the multiGet function of
   * ElasticSearch. Therefore binding the XML documents from TextGrid to Java classes using JAXB
   *
   * @param uris unique identifier for the textgrid documents
   * @param response
   * @param pathNeeded determines if a document has a path or not
   * @return returns the response containing the metadata set
   */

  public Response metaData4Uris(Collection<String> uris, Response response, boolean pathNeeded) {
    return metaData4Uris(uris, response, pathNeeded, false);
  }

  /**
   * Generating a list with all metadata for each Uri. Thereby using the multiGet function of
   * ElasticSearch. Therefore binding the XML documents from TextGrid to Java classes using JAXB
   *
   * possibility to also retrieve items from public repo, if requested
   *
   * @param uris unique identifier for the textgrid documents
   * @param response
   * @param pathNeeded determines if a document has a path or not
   * @param alsoPublic determines if the document is public and non-public available
   * @return response returns the response containing the metadata set
   */
  public Response metaData4Uris(Collection<String> uris, Response response, boolean pathNeeded, boolean alsoPublic) {

    Response responseTmp = response;

    if (uris.size() < 1) {
      return response;
    }

    int itemCounter = 0;

    MultiGetRequest metadata4UrisRequest =  new MultiGetRequest();

    for (String id : uris) {
      itemCounter++;

      if (itemCounter >= getItemLimit()) {
        log.info("\nItemLimit " + getItemLimit() + " reached.\n" + uris.size() +
            " are to many items. Just the first " + getItemLimit() +
            " datasets will be listed.\n");
        break;
      }

      String[] includes = new String[] {
          TGSearchImplConstants.ES_ORIGINAL_METADATA_FIELD,
          TGSearchImplConstants.TG_UNIQUE_IDENTIFIER_FIELD,
          TGSearchImplConstants.TG_SANDBOX_FIELD};

      String[] excludes = Strings.EMPTY_ARRAY;

      FetchSourceContext fetchSourceContext =
          new FetchSourceContext(true, includes, excludes);

      String formatedId = id.replaceAll("textgrid:", "");

      metadata4UrisRequest.add(
          new MultiGetRequest.Item(this.esIndex, formatedId)
              .fetchSourceContext(fetchSourceContext));

      if (alsoPublic) {
        metadata4UrisRequest.add(
            new MultiGetRequest.Item(this.esPublicIndex, formatedId)
                .fetchSourceContext(fetchSourceContext));
      }
    }

    MultiGetResponse metadata4allUris;
    try {
      metadata4allUris = esClient.mget(metadata4UrisRequest, RequestOptions.DEFAULT);
      responseTmp = xmlContentForQuery(response, metadata4allUris, pathNeeded);
    } catch (IOException e) {
      log.error("Error executing multiget request to elasticsearch ", e);
    }
    return responseTmp;
  }

  public Response metaData4UrisOriginalOrdered(LinkedHashMap<String, String> uriMap,
      Response response, List<String> allowedUris) {
    return this.metaData4UrisOriginalOrdered(uriMap, response, allowedUris, false);
  }

  /**
   * Returns metatadata for uris in the keeping their order, useful for listing aggregations
   *
   * @param uriMap
   * @param response
   * @param allowedUris list off uris where metadata is viewable by user (filtered by tgauth)
   * @param alsoPublic  also look into public metadata (for listing public ojects in nonpublic aggregations in lab)
   * @return
   */
  public Response metaData4UrisOriginalOrdered(LinkedHashMap<String, String> uriMap,
      Response response, List<String> allowedUris, boolean alsoPublic) {

    Response responseTmp = response;

    if (uriMap.size() < 1) {
      return response;
    }

    int itemCounter = 0;

    MultiGetRequest metadata4UrisRequest =  new MultiGetRequest();

    for (Entry<String, String> entry : uriMap.entrySet()) {
      itemCounter++;

      String id = entry.getKey();

      if (itemCounter >= getItemLimit()) {
        log.error("\nItemLimit " + getItemLimit() + " reached.\n" + uriMap.size() +
            " are to many items. Just the first " + getItemLimit() +
            " datasets will be listed.\n");
        break;
      }

      String formatedId = id.replaceAll("textgrid:", "");

      ArrayList<String> fields = new ArrayList<String>();
      fields.add("textgridUri");
      if (allowedUris.contains(id)) {
        fields.add(TGSearchImplConstants.ES_ORIGINAL_METADATA_FIELD);
      }

      String[] excludes = Strings.EMPTY_ARRAY;

      FetchSourceContext fetchSourceContext =
          new FetchSourceContext(true, fields.toArray(new String[fields.size()]), excludes);

      metadata4UrisRequest.add(
          new MultiGetRequest.Item(this.esIndex, formatedId)
              .fetchSourceContext(fetchSourceContext));

      if (alsoPublic) {
        metadata4UrisRequest.add(
            new MultiGetRequest.Item(this.esPublicIndex, formatedId)
                .fetchSourceContext(fetchSourceContext));
      }

    }

    MultiGetResponse metadata4allUris;
    try {
      metadata4allUris = esClient.mget(metadata4UrisRequest, RequestOptions.DEFAULT);
      responseTmp =
          xmlContentForQueryOriginalOrdered(response, metadata4allUris, allowedUris, uriMap);
    } catch (IOException e) {
      log.error("Error executing multiget request to elasticsearch ", e);
    }

    return responseTmp;
  }


  /**
   * querying the specific index for the original data, if it exists
   *
   * @param id
   * @return returns the content of metadata related fulltext document in original form (e.g. TEI
   *         document) encoded in Base64
   * @throws IOException
   */

  public String getOriginalDataObject(String id) throws IOException {

    GetResponse originalDataObject =
        querySettings(id, TGSearchImplConstants.ES_ORIGINAL_DATA_FIELD);

    if (!originalDataObject.isExists()
        || (originalDataObject.getSourceAsMap().get(TGSearchImplConstants.ES_ORIGINAL_DATA_FIELD) == null)) {
      log.debug("no content am empty string will be returned");
      return "";
    }

    byte[] obj = Base64.getDecoder().decode(
        originalDataObject.getSourceAsMap()
        .get(TGSearchImplConstants.ES_ORIGINAL_DATA_FIELD).toString());

    return new String(obj);

  }

  public GetResponse querySettings(String id, String fields) {

    String[] includes = new String[]{ fields };
    String[] excludes = Strings.EMPTY_ARRAY;
    FetchSourceContext fetchSourceContext =
        new FetchSourceContext(true, includes, excludes);

    GetRequest getRequest = new GetRequest(this.getEsIndex(), id)
        .fetchSourceContext(fetchSourceContext);

    GetResponse queryContent = null;
    try {
      queryContent = esClient.get(getRequest, RequestOptions.DEFAULT);
    } catch (IOException e) {
      log.error("error executing get request for id: " + id, e);
    }

    return queryContent;
  }



  public Response xmlContentForQueryOriginalOrdered(Response response,
      MultiGetResponse metadata4allUris, List<String> allowedUris,
      LinkedHashMap<String, String> uriMap) {

    /**
     * In this function each base64 attached metadata attachment will be encoded for building the
     * result XML code containing the metadata responding to a specific Uri. In this section the
     * base64 encoded XML attachements of the metadata and TEIs will be decoded to generate the XML
     * tree for the response (in XML)
     */

    Response tmp = response;
    Unmarshaller uriMetadataJavaObject;

    // keep track of uris already added to output to prevent double entries in case of entries in public & nonpublic index
    Set<String> alreadyAdded = new HashSet<String>();

    /**
     * The variable uriMetadataContext is the preparation to save the metadata corresponding to a
     * uri in a XML document The uriMetadataJavaObject makes the other direction
     */

    try {
      uriMetadataJavaObject = this.jaxbContext.createUnmarshaller();

      for (MultiGetItemResponse metadata4allUrisResp : metadata4allUris.getResponses()) {

        if (metadata4allUrisResp.getResponse().isExists()) {

          Map<String, Object> metadata4allUrisContent = metadata4allUrisResp.getResponse().getSource();

          String hitUri = metadata4allUrisContent.get("textgridUri").toString();
          ResultType rt = new ResultType();

          if (allowedUris.contains(hitUri)) {
            String metadata4allUrisContentString = metadata4allUrisContent
                .get(TGSearchImplConstants.ES_ORIGINAL_METADATA_FIELD).toString();
            ObjectType mdcont = metdataJaxbFromString(uriMetadataJavaObject, metadata4allUrisContentString);
            rt.setObject(mdcont);
            if (metadata4allUrisContent.containsKey(TGSearchImplConstants.TG_SANDBOX_FIELD)) {

              rt.setSandbox(Boolean.valueOf(metadata4allUrisContent
                  .get(TGSearchImplConstants.TG_SANDBOX_FIELD).toString()));
            }
          } else {
            rt = TGSearchUtils.createUriOnlyResult(hitUri);
          }
          // TODO: uri from aggregation here

          String originalUri = uriMap.get(hitUri);
          rt.setTextgridUri(originalUri);

          if(!alreadyAdded.contains(hitUri)) {
            tmp.getResult().add(rt);
            alreadyAdded.add(hitUri);
          } else {
            log.debug("skipped double entry " + hitUri);
          }
        }
      }

    } catch (JAXBException e) {
      log.error("metaData4Uris: could not build XML", e);
    } catch (XMLStreamException e) {
      log.error("metaData4Uris: could not read XML", e);
    } catch (FactoryConfigurationError e) {
      log.error("metaData4Uris: FactoryConfigurationerror", e);
    }

    return tmp;
  }

  public Response xmlContentForQuery(Response response, MultiGetResponse metadata4allUris,
      Boolean pathNeeded) {

    /**
     * In this function each base64 attached metadata attachment will be encoded for building the
     * result XML code containing the metadata responding to a specific Uri. In this section the
     * base64 encoded XML attachements of the metadata and TEIs will be decoded to generate the XML
     * tree for the response (in XML)
     */

    Response tmp = response;
    Unmarshaller uriMetadataJavaObject;

    /**
     * The variable uriMetadataContext is the preparation to save the metadata corresponding to a
     * uri in a XML document The uriMetadataJavaObject makes the other direction
     */
    try {
      uriMetadataJavaObject = this.jaxbContext.createUnmarshaller();

      for (MultiGetItemResponse metadata4allUrisResp : metadata4allUris.getResponses()) {

        if (metadata4allUrisResp.getResponse() != null) {

          ResultType result = new ResultType();
          String tguri;

          Map<String, Object> metadata4allUrisContent = metadata4allUrisResp.getResponse().getSource();
          if(metadata4allUrisContent == null) {
            log.error("[CONSISTENCY PROBLEM] no content for id: " + metadata4allUrisResp.getId());
            continue;
          }

          Object uriField = metadata4allUrisContent.get(TGSearchImplConstants.TG_UNIQUE_IDENTIFIER_FIELD);

          if(uriField != null) {
            tguri = uriField.toString();
          } else {
            tguri = "textgrid:" + metadata4allUrisResp.getId();
            log.error("[CONSISTENCY PROBLEM] no uri, generating from id: " + tguri);
          }

          Object mdField = metadata4allUrisContent.get(TGSearchImplConstants.ES_ORIGINAL_METADATA_FIELD);
          if(mdField != null) {
            String metadata4allUrisContentString = mdField.toString();
            ObjectType mdcont = metdataJaxbFromString(uriMetadataJavaObject, metadata4allUrisContentString);
            result.setObject(mdcont);
          } else {
            log.error("[CONSISTENCY PROBLEM] no original metadata field for: " + tguri);
            result = TGSearchUtils.createUriOnlyResult(tguri);
          }

          // TODO: uri from aggregation here
          result.setTextgridUri(tguri);

          if (metadata4allUrisContent.containsKey(TGSearchImplConstants.TG_SANDBOX_FIELD)) {
            result.setSandbox(Boolean.valueOf(metadata4allUrisContent
              .get(TGSearchImplConstants.TG_SANDBOX_FIELD).toString()));
          }

          if (pathNeeded) {
            result.setPathResponse(path4TGElement.listPath(metadata4allUrisContent
              .get(TGSearchImplConstants.TG_UNIQUE_IDENTIFIER_FIELD).toString()));
          }

          response.getResult().add(result);
        }
      }

    } catch (JAXBException e) {
      log.error("metaData4Uris: could not build XML", e);
    } catch (XMLStreamException e) {
      log.error("metaData4Uris: could not read XML", e);
    } catch (FactoryConfigurationError e) {
      log.error("metaData4Uris: FactoryConfigurationerror", e);
    }

    return tmp;
  }

  public ObjectType metdataJaxbFromString(Unmarshaller unmarshaller,
      String xmlString) throws XMLStreamException {

    byte[] bytes = Base64.getDecoder().decode(xmlString);

    ObjectType res = new ObjectType();
    InputStream is = new ByteArrayInputStream(bytes);
    XMLStreamReader streamr = this.xmlInputFactory.createXMLStreamReader(is);
    try{
      JAXBElement<MetadataContainerType> mdcont = unmarshaller
        .unmarshal(streamr, MetadataContainerType.class);
      res = mdcont.getValue().getObject();
    } catch(JAXBException e) {
      log.error("metaData4Uris: could not build XML", e);
    } finally {
      streamr.close();
    }
    return res;
  }

  // ----------------------------------- getter and setter ----------------------------------------


  public static String getUrl() {
    return url;
  }

  public static void setUrl(String url) {
    TGElasticSearchClient.url = url;
  }

  public static int[] getPort() {
    return ports;
  }

  public static void setPorts(int ports[]) {
    TGElasticSearchClient.ports = ports;
  }

  public RestHighLevelClient getEsClient() {
    return esClient;
  }

  public void setEsClient(RestHighLevelClient esClient) {
    TGElasticSearchClient.esClient = esClient;
  }


  public String getEsIndex() {
    return esIndex;
  }


  public void setEsIndex(String esIndex) {
    this.esIndex = esIndex;
  }

  /**
   * The public index, useful for searchAllProejects in nonpublic search
   *
   * @return esPublicIndex
   */
  public String getEsPublicIndex() {
    return esPublicIndex;
  }

  /**
   * The public index, useful for searchAllProejects in nonpublic search
   *
   * @param esPublicIndex
   */
  public void setEsPublicIndex(String esPublicIndex) {
    this.esPublicIndex = esPublicIndex;
  }

  public static int getItemLimit() {
    return itemLimit;
  }

  public static void setItemLimit(int itemLimit) {
    TGElasticSearchClient.itemLimit = itemLimit;
  }

  public JAXBContext jaxbContext() {
    return jaxbContext;
  }

  public void setFederatedSparqlClient(SesameClient federatedSparqlClient) {
    this.federatedSparqlClient = federatedSparqlClient;
  }

  public List<String> getUntouchedFieldExceptions() {
    return untouchedFieldExceptions;
  }

  public void setUntouchedFieldExceptions(String[] untouchedFieldExceptions) {
    this.untouchedFieldExceptions = Arrays.asList(untouchedFieldExceptions);
  }

}
