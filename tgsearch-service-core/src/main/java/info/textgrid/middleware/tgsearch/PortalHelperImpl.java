/*
 * #%L TextGrid :: TGSearch :: Core
 *
 * %% Copyright (C) 2011-2020 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen
 * (http://www.sub.uni-goettingen.de) %%
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L%
 */

package info.textgrid.middleware.tgsearch;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.ExecutionException;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.action.search.MultiSearchRequest;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.Strings;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.script.Script;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.BucketOrder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket;
import org.elasticsearch.search.aggregations.metrics.TopHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import info.textgrid.middleware.tgsearch.api.PortalHelper;
import info.textgrid.middleware.tgsearch.auth.AuthClient;
import info.textgrid.middleware.tgsearch.utils.TGSearchUtils;
import info.textgrid.namespaces.metadata.portalconfig._2020_06_16.Portalconfig;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.portal.Project;
import info.textgrid.namespaces.middleware.tgsearch.portal.ProjectsResponse;

/**
 * some helper functions used by the tgrep portal
 * @author Ubbo Veentjer
 *
 */
public class PortalHelperImpl implements PortalHelper {

  public static final int MAX_BUCKETS = 10000;
  public static final int MAX_FILES_PER_AGGREGATION = 100; // adjust this if new portalconfigs are not shown on https://textgridrep.org/projects anymore
  public static final String PROJECT_AGGREGATION = "projects";
  public static final String PROJECT_ID_FIELD = "project.id";
  public static final String PROJECT_NAME_FIELD = "project.value.untouched";
  public static final String PROJECT_SPLIT_CHAR = "#";
  public static final String PORTALCONFIG_MIMETYPE = "text/tg.portalconfig+xml";

  private Log log = LogFactory.getLog(PortalHelperImpl.class);
  private TGElasticSearchClient tgEsclient;
  private AuthClient authClient;
  private XMLInputFactory xmlInputFactory;

  private LoadingCache<String, TreeSet<String>> toplevelCache;
  private LoadingCache<String, TreeSet<String>> toplevelSandboxCache;

  public PortalHelperImpl(TGElasticSearchClient tgesClient, AuthClient authClient) {
    this.tgEsclient = tgesClient;
    this.authClient = authClient;
    this.xmlInputFactory = XMLInputFactory.newInstance();

    String spec = "maximumSize=1000,expireAfterWrite=20m";
    if(this.authClient.isProjectAuth()) {
      // nonpublic data may change more frequent
      spec = "maximumSize=1000,expireAfterWrite=30s";
    }
    this.toplevelCache =
        CacheBuilder.from(spec)
          .build(new CacheLoader<String, TreeSet<String>>() {
            @Override
            public TreeSet<String> load(String projectId) throws Exception {
              return getTopLevelUris(projectId, false);
            }
          });

    this.toplevelSandboxCache =
        CacheBuilder.from(spec)
          .build(new CacheLoader<String, TreeSet<String>>() {
            @Override
            public TreeSet<String> load(String projectId) throws Exception {
              return getTopLevelUris(projectId, true);
            }
          });

  }

  @Override
  @GET
  @Path("/projects")
  @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public ProjectsResponse listProjects(
      @QueryParam("sid") String sid,
      @QueryParam("sandbox") @DefaultValue("false") final boolean sandbox
      ){

    ProjectsResponse response = new ProjectsResponse();

    QueryBuilder projectFilter = null;
    if (authClient.isProjectAuth()) {
      log.debug("query auth for searchable projects");
      // get list of projects browseable by user
      List<String> projectsAllowedToSearch = authClient
          .getProjectsForSID(sid);
      projectFilter = QueryBuilders.termsQuery("project.id", projectsAllowedToSearch);
      // TODO: a message could be send to lab if list is empty,
      // no need for searching then
      if (projectsAllowedToSearch.isEmpty()) {
        log.debug("Auth enabled, but no searchable Projects found for this user");
        return response;
      }
    }

    SearchRequest portalConfigSearchRequest = this.createPortalConfigSearchRequest(sandbox, "", projectFilter);
    SearchRequest projectSearchRequest = this.createProjectsFacetRequest(sandbox, projectFilter);

    MultiSearchRequest mr = new MultiSearchRequest()
        .add(portalConfigSearchRequest)
        .add(projectSearchRequest);

    try {
      MultiSearchResponse msearchResponse = this.tgEsclient.getEsClient().msearch(mr, RequestOptions.DEFAULT);
      // https://www.elastic.co/guide/en/elasticsearch/reference/6.8/search-multi-search.html -> responses are in same order as request
      SearchResponse portalConfigResponse = msearchResponse.getResponses()[0].getResponse();
      SearchResponse projectsResponse = msearchResponse.getResponses()[1].getResponse();

      // create entries for all projects which have a projectConfig
      Map<String, Project> pcMap = handlePortalConfigResponse(portalConfigResponse, sandbox);

      Terms projectTerms = (Terms)projectsResponse.getAggregations().asMap().get(PROJECT_AGGREGATION );
      for (Bucket bucket : projectTerms.getBuckets()) {
        Project project;

        String[] projectArr = bucket.getKeyAsString().split(PROJECT_SPLIT_CHAR, 2);
        String projectId = projectArr[0];
        String projectName = projectArr[1];

        // use existing project with portalconfig entries if available
        if(pcMap.containsKey(projectId)) {
          project = pcMap.get(projectId);
        } else {
          project = new Project()
              .setId(projectId)
              .setName(projectName);
        }
        project.setCount(bucket.getDocCount());
        response.addProject(project);
      }

    } catch (IOException e) {
      log.error(e);
    }

    return response;

  }

  @Override
  @GET
  @Path("/project/{id}")
  @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public Project projectDetails(
      @QueryParam("sid") String sid,
      @QueryParam("sandbox") @DefaultValue("false") final boolean sandbox,
      @PathParam("id") final String id) {

    Project response = new Project();

    List<String> projectsAllowedToSearch = new ArrayList<String>();
    if (authClient.isProjectAuth()) {
      log.debug("query auth for searchable projects");
      // get list of projects browseable by user
      projectsAllowedToSearch = authClient
          .getProjectsForSID(sid);
      // if project requested not in list of allowed projects, return empty result
      if(!projectsAllowedToSearch.contains(id)) {
        return response;
      }
    }
    TermsQueryBuilder projectFilter = QueryBuilders.termsQuery("project.id", projectsAllowedToSearch);

    SearchRequest portalConfigSearchRequest = this.createPortalConfigSearchRequest(sandbox, id, projectFilter);
    SearchRequest readmeSearchRequest = this.createReadmeSearchRequest(sandbox, id, projectFilter);
    SearchRequest projectSearchRequest = this.createProjectIdRequest(id); // the fallback

    MultiSearchRequest mr = new MultiSearchRequest()
        .add(portalConfigSearchRequest)
        .add(readmeSearchRequest)
        .add(projectSearchRequest);

    try {
      MultiSearchResponse msearchResponse = this.tgEsclient.getEsClient().msearch(mr, RequestOptions.DEFAULT);
      // https://www.elastic.co/guide/en/elasticsearch/reference/6.8/search-multi-search.html -> responses are in same order as request
      SearchResponse portalConfigResponse = msearchResponse.getResponses()[0].getResponse();
      SearchResponse readmeResponse = msearchResponse.getResponses()[1].getResponse();

      Map<String, Project> pcMap = handlePortalConfigResponse(portalConfigResponse, sandbox);
      response = pcMap.get(id);

      // fallback to project name from rbac (via tgmetadata) if no projectconfig.xml found
      if(response == null) {
        SearchResponse projectResponse = msearchResponse.getResponses()[2].getResponse();
        Map<String, Object> smap = projectResponse.getHits().getAt(0).getSourceAsMap();
        String projectId = ((Map<String,Object>)smap.get("project"))
            .get("id").toString();
        String projectName = ((Map<String,Object>)smap.get("project"))
            .get("value").toString();
        response = new Project().setId(projectId).setName(projectName);
      }

      // now find the toplevel readme
      TreeSet<String> toplevelMarkdownUris = findTopLevelUrisForFormat(sandbox, "text/markdown");
      Terms readmeTerms = (Terms)readmeResponse.getAggregations().asList().get(0);
      for (Bucket bucket : readmeTerms.getBuckets()) {
        TopHits tophit = (TopHits) bucket.getAggregations().asList().get(0);
        Map<String, Object> smap = tophit.getHits().getAt(0).getSourceAsMap();
        String readmeUri = smap
            .get(TGSearchImplConstants.TG_UNIQUE_IDENTIFIER_FIELD).toString();
        if(toplevelMarkdownUris.contains(readmeUri)) {
          String markdown = smap
              .get(TGSearchImplConstants.ES_FULLTEXT_FIELD).toString();
          response.setReadme(markdown);
          response.setReadmeUri(readmeUri);
        }
      }
    } catch (IOException e) {
      log.error(e);
    }
    return response;
  }

  @Override
  @GET
  @Path("/toplevel/{id}")
  @Produces({"application/xml", "text/xml"})
  public Response listToplevelAggregations(
      @PathParam("id") String id,
      @QueryParam("sid") String sid,
      @QueryParam("start") @DefaultValue("0") int start,
      @QueryParam("limit") @DefaultValue("10") int limit,
      @QueryParam("sandbox") @DefaultValue("false") final boolean sandbox) {

    Response response = new Response();

    if (authClient.isProjectAuth()) {
      log.debug("query auth for searchable projects");
      // get list of projects browseable by user
      List<String> projectsAllowedToSearch = authClient
          .getProjectsForSID(sid);
      // if project requested not in list of allowed projects, return empty result
      if(!projectsAllowedToSearch.contains(id)) {
        return response;
      }
    }

    List<String> uris;
    try {
      if(sandbox) {
        uris = new ArrayList<String>(this.toplevelSandboxCache.get(id));
      } else {
        uris = new ArrayList<String>(this.toplevelCache.get(id));
      }
      response.setHits(Integer.toString(uris.size()));
      int end = start+limit > uris.size() ? uris.size() : start+limit;
      if(start <= uris.size()) {
        List<String> subset = uris.subList(start, end);
        response = this.tgEsclient.metaData4Uris(subset, response);
      }
    } catch (ExecutionException e) {
      log.error(e);
    }
    return response;
  }

  private TreeSet<String> getTopLevelUris(String projectId, boolean sandbox) {
    HashMap<String, Object> tmplOptions = new HashMap<String, Object>();
    tmplOptions.put("projectId", projectId);
    if (!sandbox) {
      tmplOptions.put("sandboxRestriction", "FILTER NOT EXISTS { ?s tg:isNearlyPublished ?inp } .");
    }
    String query = TGSearchUtils.createQueryFromTemplate("listTopLevel_public_project.tmpl", tmplOptions);
    TreeSet<String> uris = this.tgEsclient.getUrisFromQuery(query);
    return uris;
  }

  private Map<String, Project> handlePortalConfigResponse(SearchResponse portalConfigResponse, boolean sandbox) {

    TreeSet<String> toplevelProjectConfigUris = findTopLevelUrisForFormat(sandbox, PORTALCONFIG_MIMETYPE);
    Map<String, Project> pcMap = new HashMap<String, Project>();
    Terms pcTerms = (Terms)portalConfigResponse.getAggregations().asList().get(0);

    Unmarshaller pcUnmarshaller;
    try {
      pcUnmarshaller = this.tgEsclient.jaxbContext().createUnmarshaller();

      for (Bucket bucket : pcTerms.getBuckets()) {
        TopHits tophit = (TopHits) bucket.getAggregations().asList().get(0);
        Map<String, Object> smap = tophit.getHits().getAt(0).getSourceAsMap();

        String tguri = smap
            .get(TGSearchImplConstants.TG_UNIQUE_IDENTIFIER_FIELD).toString();
        String projectId = ((Map<String,Object>)smap.get("project"))
            .get("id").toString();
        String projectName = ((Map<String,Object>)smap.get("project"))
            .get("value").toString();

        if(toplevelProjectConfigUris.contains(tguri)) {
          Object pcField = smap.get(TGSearchImplConstants.ES_ORIGINAL_DATA_FIELD);
          if(pcField != null) {

            Project project = new Project()
                .setId(projectId)
                .setPortalconfigUri(tguri);

            String pcContentString = pcField.toString();
            byte[] bytes = Base64.getDecoder().decode(pcContentString);
            InputStream is = new ByteArrayInputStream(bytes);
            XMLStreamReader streamr = xmlInputFactory.createXMLStreamReader(is);
            try {
              JAXBElement<Portalconfig> portalconfig = pcUnmarshaller
                  .unmarshal(streamr, Portalconfig.class);
              project.setPortalconfig(portalconfig.getValue());
              // prefer project name from config, fallback to name from RBAC
              if(portalconfig.getValue().getName() != null) {
                project.setName(portalconfig.getValue().getName());
              } else {
                project.setName(projectName);
              }
            } catch (JAXBException e) {
              log.error("projectConfig: could not build XML", e);
            } finally {
              streamr.close();
            }

            pcMap.put(projectId, project);
          } else {
            log.error("[CONSISTENCY PROBLEM] no original data field for: " + tguri);
          }
        }
      }
    } catch (JAXBException e) {
      log.error("projectConfig: could not build XML", e);
    } catch (XMLStreamException e) {
      log.error("projectConfig: could not read XML", e);
    } catch (FactoryConfigurationError e) {
      log.error("projectConfig: FactoryConfigurationerror", e);
    }

    return pcMap;
  }

  private TreeSet<String> findTopLevelUrisForFormat(boolean sandbox, String format) {
    HashMap<String, Object> tmplOptions = new HashMap<String, Object>();
    tmplOptions.put("format", format);
    if (!sandbox) {
      tmplOptions.put("sandboxRestriction", "FILTER NOT EXISTS { ?s tg:isNearlyPublished ?inp } .");
    }
    String query = TGSearchUtils.createQueryFromTemplate("listTopLevelObjectsForFormat.tmpl", tmplOptions);
    TreeSet<String> toplevelMarkdownUris = this.tgEsclient.getUrisFromQuery(query);
    return toplevelMarkdownUris;
  }

  private SearchRequest createProjectsFacetRequest(boolean sandbox, QueryBuilder projectFilter) {

    BoolQueryBuilder queryfilter = QueryBuilders.boolQuery();
    if(this.authClient.isProjectAuth()) {
      queryfilter.must(projectFilter);
    }

    SearchRequest searchRequest = new SearchRequest(this.tgEsclient.getEsIndex());
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

    BucketOrder bucketOrder = BucketOrder.count(false);

    Script script = new Script("doc['"+PROJECT_ID_FIELD+"'].value + '"+PROJECT_SPLIT_CHAR+"' +  doc['"+PROJECT_NAME_FIELD+"'].value");

    AggregationBuilder agg = AggregationBuilders
        .terms(PROJECT_AGGREGATION)
        .script(script)
        .order(bucketOrder)
        .size(MAX_BUCKETS);
    searchSourceBuilder.aggregation(agg);

    if (!sandbox) {
      queryfilter.mustNot(QueryBuilders.existsQuery("nearlyPublished"));
    }

    searchSourceBuilder.query(queryfilter);
    searchSourceBuilder.size(0);
    //System.out.println(Strings.toString(searchSourceBuilder, true, true));
    searchRequest.source(searchSourceBuilder);
    return searchRequest;

  }

  /**
   * Build a searchrequest for latest revisions of portalconfig.xml files with mimetype
   * PORTALCONFIG_MIMETYPE. Optionally only in project with id projectId (if not empty String),
   * optionally including sandbox results. Searchresult shall contain original data field.
   *
   * @param sandbox       whether to include sandbox results
   * @param projectId     optional projectid to limit search to one project
   * @param projectFilter optional project filter, if authentication is required for searching own projects
   * @return
   */
  private SearchRequest createPortalConfigSearchRequest(boolean sandbox, String projectId, QueryBuilder projectFilter) {
    String[] includeFields = new String[] {
        TGSearchImplConstants.TG_UNIQUE_IDENTIFIER_FIELD,
        TGSearchImplConstants.ES_ORIGINAL_DATA_FIELD,
        "project",
        "revision"
    };
    return createSpecificFileSearchRequest(PORTALCONFIG_MIMETYPE, "portalconfig.xml", includeFields, sandbox, projectId, projectFilter);
  }

  /**
   * Build a searchrequest for latest revisions of README.md files with mimetype
   * text/markdown. Optionally only in project with id projectId (if not empty String),
   * optionally including sandbox results.Searchresult shall contain fulltext field.
   *
   * @param sandbox       whether to include sandbox results
   * @param projectId     optional projectid to limit search to one project
   * @param projectFilter optional project filter, if authentication is required for searching own projects
   * @return
   */
  private SearchRequest createReadmeSearchRequest(boolean sandbox, String projectId, QueryBuilder projectFilter) {
    String[] includeFields = new String[] {
        TGSearchImplConstants.TG_UNIQUE_IDENTIFIER_FIELD,
        TGSearchImplConstants.ES_FULLTEXT_FIELD,
        "project",
        "revision"
    };
    return createSpecificFileSearchRequest("text/markdown", "README.md", includeFields, sandbox, projectId, projectFilter);
  }

  /**
   * Build a searchrequest for latest revisions of a file with title TITLE and format FORMAT
   * Optionally only in project with id projectId (if not empty String),
   * optionally including sandbox results. Searchresult shall contain original data field.
   *
   * @param format        format of object to search for
   * @param title         title of object to search for
   * @param includeFields fields to return from elasticsearch
   * @param sandbox       whether to include sandbox results
   * @param projectId     optional projectid to limit search to one project
   * @param projectFilter optional project filter, if authentication is required for searching own projects
   * @return
   */
  private SearchRequest createSpecificFileSearchRequest(String format, String title, String[] includeFields, boolean sandbox, String projectId,
      QueryBuilder projectFilter) {

    SearchRequest searchRequest = new SearchRequest(this.tgEsclient.getEsIndex());
    BoolQueryBuilder queryfilter = QueryBuilders.boolQuery();

    if(projectId != "") {
      queryfilter.must(QueryBuilders.termQuery("project.id", projectId));
    }
    if(this.authClient.isProjectAuth()) {
      queryfilter.must(projectFilter);
    }

    queryfilter.must(QueryBuilders.termQuery("format.untouched", format));
    queryfilter.must(QueryBuilders.termQuery("title.untouched", title));
    if (!sandbox) {
      queryfilter.mustNot(QueryBuilders.existsQuery("nearlyPublished"));
    }

    // build the query
    BoolQueryBuilder finalquery = QueryBuilders.boolQuery()
      .filter(queryfilter);

    FieldSortBuilder sort = SortBuilders.fieldSort("revision").order(SortOrder.DESC);

    AggregationBuilder lr = AggregationBuilders
        .topHits("last_revision_hits")
        .sort(sort)
        .size(1)
        .fetchSource(includeFields, Strings.EMPTY_ARRAY);

    AggregationBuilder agg = AggregationBuilders
        .terms("last_revision")
        .field("baseUri.untouched")
        .size(MAX_FILES_PER_AGGREGATION)
        .subAggregation(lr);

    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    searchSourceBuilder.query(finalquery).from(0).size(0);
    searchSourceBuilder.aggregation(agg);
    searchRequest.source(searchSourceBuilder);
    //System.out.println(Strings.toString(searchSourceBuilder, true, true));
    return searchRequest;
  }

  /**
   * Search the project title for a given project ID, a fallback method if no projectconfig
   * is available
   *
   * @param projectId the project ID
   * @return
   */
  private SearchRequest createProjectIdRequest(String projectId) {
    SearchRequest searchRequest = new SearchRequest(this.tgEsclient.getEsIndex());
    TermQueryBuilder query = QueryBuilders.termQuery("project.id", projectId);
    String[] includeFields = new String[] {
        "project",
    };
    FetchSourceContext fetchSourceContext =
        new FetchSourceContext(true, includeFields, Strings.EMPTY_ARRAY);
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    searchSourceBuilder.query(query).from(0).size(1).fetchSource(fetchSourceContext);
    searchRequest.source(searchSourceBuilder);
    return searchRequest;
  }

}
