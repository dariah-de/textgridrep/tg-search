/*
 * #%L TextGrid :: TGSearch :: Core
 *
 * %% Copyright (C) 2011-2019 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen
 * (http://www.sub.uni-goettingen.de) %%
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L%
 */

package info.textgrid.middleware.tgsearch;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.StreamingOutput;
import javax.xml.stream.XMLInputFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.Strings;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import info.textgrid.middleware.tgsearch.api.Info;
import info.textgrid.middleware.tgsearch.auth.AuthClient;
import info.textgrid.middleware.tgsearch.utils.TGSearchUtils;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgsearch.PathResponse;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.Revisions;
import info.textgrid.namespaces.middleware.tgsearch.TextgridUris;
import info.textgrid.utils.sesameclient.SesameClient;
import info.textgrid.utils.sesameclient.TGSparqlUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

public class ElasticSearchInfoImpl implements Info {

  private AuthClient authClient;
  private Log log = LogFactory.getLog(TGElasticSearchClient.class);
  private SesameClient sparqlClient;
  private TGElasticSearchClient tgElasticSearchClient;
  private PathBuilder pathBuilder;
  private XMLInputFactory xmlInputFactory;


  // private int itemLimit=100;

  /**
   * Constructor
   *
   * @param tgElasticSearchClient contains port number and URL of the ElasticSearch client.
   * @param authClient
   * @param sparqlClient Endpoint of the Sesame Client
   */

  public ElasticSearchInfoImpl(TGElasticSearchClient tgElasticSearchClient, AuthClient authClient,
      SesameClient sparqlClient) {

    this.tgElasticSearchClient = tgElasticSearchClient;
    this.authClient = authClient;
    this.sparqlClient = sparqlClient;
    this.pathBuilder = new PathBuilder(tgElasticSearchClient);
    this.xmlInputFactory = XMLInputFactory.newInstance();
  }

  /*
   * (non-Javadoc)
   *
   * @see info.textgrid.middleware.tgsearch.Info#metadata(java.lang.String, java.lang.String)
   */

  @Override
  @GET
  @Path("/{uri}")
  @Produces({"application/xml", "text/xml"})
  @Operation(
        summary = "search metadata",
        description = "tgsearch response with metadata for single TextGrid object",
        responses = {
            @ApiResponse(
                responseCode = "200",
                content = @Content(mediaType = "application/xml")
            )
        },
        parameters = {
                    @Parameter(name = "uri",
                            required = true,
                            example = "textgrid:1234.0"
                            )
                      }
  )
  public Response metadata(
      @PathParam("uri") String uri,
      @QueryParam("sid") @DefaultValue("") String sid,
      @QueryParam("path") @DefaultValue("false") String path) {

    String uriTmp = uri;
    Response rootnode = new Response();

    if (!uriTmp.startsWith(TGSearchImplConstants.TG_URI_PREFIX)) {
      uriTmp = TGSearchImplConstants.TG_URI_PREFIX + uriTmp;
    }

    if (!uriTmp.contains(".")) {
      try {
        uriTmp = TGSparqlUtils.getLatestUri(sparqlClient, uriTmp);
      } catch (IOException e) {
        log.debug("no revision uri found for " + uriTmp);
        return rootnode;
      }
    }

    List<String> uris = new ArrayList<String>();
    uris.add(uriTmp);

    uris = authClient.filterUris(sid, uris);

    Response response = new Response();
    if(this.authClient.isProjectAuth()) {
      response = this.tgElasticSearchClient.metaData4Uris(uris, rootnode, Boolean.parseBoolean(path), true);
    } else {
      response = this.tgElasticSearchClient.metaData4Uris(uris, rootnode, Boolean.parseBoolean(path));
    }
    return response;
  }


  /*
   * (non-Javadoc)
   *
   * @see info.textgrid.middleware.tgsearch.Info#revisions(java.lang.String)
   */

  @Override
  @GET
  @Path("/{uri}/revisions")
  @Produces({"application/xml", "text/xml"})
  @Operation(
        summary = "list of revisions",
        description = "list of revisions for a single TextGrid object",
        responses = {
            @ApiResponse(
                responseCode = "200",
                content = @Content(mediaType = "application/xml")
            )
        }
  )
  public Revisions revisions(@PathParam("uri") String uri) {

    String uriTmp = uri;

    if (!uriTmp.startsWith(TGSearchImplConstants.TG_URI_PREFIX)) {
      uriTmp = TGSearchImplConstants.TG_URI_PREFIX + uriTmp;
    }

    if (uriTmp.contains(".")) {
      uriTmp = uriTmp.substring(0, uri.indexOf('.'));
    }

    String query = createRevisonSparqlQuery(uriTmp);

    log.debug("sparql query for revisions at " + this.sparqlClient.getEndpoint() + " : " + query);

    InputStream in = null;
    try {
      in = this.sparqlClient.sparql(query);
    } catch (IOException e) {
      // on error in rdfstore return empty response
      // TODO: throw error to client
      log.error("error in rdfstore request", e);
      return new Revisions();
    }

    TreeSet<String> uris = TGSearchUtils.urisFromSparqlResponse(in, this.xmlInputFactory);

    Revisions response = new Revisions();
    response.setTextgridUri(uriTmp);

    for (String u : uris) {
      int rev = Integer.parseInt(u.substring(u.indexOf(".") + 1));
      response.getRevision().add(BigInteger.valueOf(rev));
    }

    return response;

  }

  @GET
  @Override
  @Path("/{uri}/revisions/meta")
  @Produces({"application/xml", "text/xml"})
  @Operation(
        summary = "list of revisions incl. metadata",
        description = "list of revisions and their metadata for a single TextGrid object",
        responses = {
            @ApiResponse(
                responseCode = "200",
                content = @Content(mediaType = "application/xml")
            )
        }
  )
  public Response revisionsAndMeta(@PathParam("uri") String uri,
      @QueryParam("sid") String sid) {

    String uriTmp = uri;

    Response response = new Response();

    if (!uriTmp.startsWith(TGSearchImplConstants.TG_URI_PREFIX)) {
      uriTmp = TGSearchImplConstants.TG_URI_PREFIX + uriTmp;
    }

    if (uriTmp.contains(".")) {
      uriTmp = uriTmp.substring(0, uriTmp.indexOf('.'));
    }

    String query = createRevisonSparqlQuery(uriTmp);

    log.debug("sparql query for revisions at " + this.sparqlClient.getEndpoint() + " : " + query);

    InputStream in;
    try {
      in = this.sparqlClient.sparql(query);
    } catch (IOException e) {
      // on error with sparql query return empty response
      // TODO: throw error to client
      log.error("error in rdfstore request", e);
      return response;
    }

    TreeSet<String> allUris = TGSearchUtils.urisFromSparqlResponse(in, this.xmlInputFactory);

    int numberOfUris = allUris.size();

    log.debug("number of uris: " + numberOfUris);
    List<String> allowedUris = authClient.filterUris(sid, allUris);


    return this.tgElasticSearchClient.metaData4Uris(allowedUris, response);

  }

  /**
   * TODO: move to tgsparqlutils in httpclients
   *
   * create a sparql query which returns uris of all (not deleted) revisions for a given baseuri
   *
   * @param baseUri
   * @return sparql query
   */

  public static String createRevisonSparqlQuery(String baseUri) {
    String query = "PREFIX ore:<http://www.openarchives.org/ore/terms/>\n" +
        "PREFIX tg:<http://textgrid.info/relation-ns#>\n" +
        "\n" +
        "SELECT ?uri\n" +
        "WHERE {\n" +
        " <" + baseUri + "> tg:isBaseUriOf ?uri\n" +
        " FILTER NOT EXISTS { ?uri tg:isDeleted true }\n" +
        "}";
    return query;
  }

  /*
   * (non-Javadoc)
   *
   * @see info.textgrid.middleware.tgsearch.Info#relations(java.lang.String)
   */

  @Override
  @GET
  @Path("/{uri}/relations")
  @Produces({"application/xml", "text/xml"})
  @Operation(
        deprecated = true
  )
  public Response relations(@PathParam("uri") String uri) {

    log.info("someone requested relations! TODO implement");
    // TODO
    // Response rootnode = new Response();
    return new Response();
  }

  /*
   * (non-Javadoc)
   *
   * @see info.textgrid.middleware.tgsearch.Info#baseline(java.lang.String)
   */

  @Override
  @GET
  @Path("/{uri}/metadata")
  @Produces({"application/xml", "text/xml"})
  @Operation(
        summary = "get metadata",
        description = "only the textgrid metatada object for given uri, not enclosed by tgsearch-response xml",
        responses = {
            @ApiResponse(
                responseCode = "200",
                content = @Content(mediaType = "application/xml")
            )
        }
  )
  public ObjectType onlyMetadata(@PathParam("uri") String uri,
      @QueryParam("sid") String sid) {

    Response resp = metadata(uri, sid, "false");
    // ObjectType md = resp.getResult().get(0).getObject();
    return resp.getResult().get(0).getObject();
  }

  @Override
  @GET
  @Path("/{uri}/path")
  @Produces({"application/xml", "text/xml"})
  @Operation(
        summary = "list path",
        description = "list path according to TextGrid metadata (edition → work → aggragation,…) for single TextGrid Object",
        responses = {
            @ApiResponse(
                responseCode = "200",
                content = @Content(mediaType = "application/xml")
            )
        }
  )
  public PathResponse getPath(
      @PathParam("uri") String uri) {

    log.debug("building path for: " + uri);
    return this.pathBuilder.listPath(uri);


  }

  @Override
  @GET
  @Path("/{uri}/baseline")
  @Produces({"application/xml", "text/xml"})
  @Operation(
      deprecated = true
  )
  public StreamingOutput baseline(@PathParam("uri") String uri,
      @QueryParam("sid") String sid) {
    // TODO Auto-generated method stub
    log.info("someone requested baseline! TODO implement");
    return null;
  }

  @Override
  @GET
  @Path("/{uri}/editionWorkMeta")
  @Produces({"application/xml", "text/xml"})
  @Operation(
        summary = "get correspdong work metadata",
        description = "edition and work metadata for given object from first matching parent edition",
        responses = {
            @ApiResponse(
                responseCode = "200",
                content = @Content(mediaType = "application/xml")
            )
        }
  )
  public Response getEditionWorkMeta(@PathParam("uri") String uri, @QueryParam("sid") String sid) {
    log.info("getting metadata, editionmeta & workmeta for: " + uri);

    String uriTmp = uri;

    Response response = new Response();

    if (!uriTmp.startsWith(TGSearchImplConstants.TG_URI_PREFIX)) {
      uriTmp = TGSearchImplConstants.TG_URI_PREFIX + uriTmp;
    }

    HashMap<String, Object> tmplOptions = new HashMap<String, Object>();
    tmplOptions.put(TGSearchImplConstants.TG_UNIQUE_IDENTIFIER_FIELD, uriTmp);
    String query =
        TGSearchUtils.createQueryFromTemplate("getEditionAndWorkForUri.tmpl", tmplOptions);
    log.info(query);

    TreeSet<String> allUris = this.tgElasticSearchClient.getUrisFromQuery(query);
    allUris.add(uriTmp);
    List<String> allowedUris = authClient.filterUris(sid, allUris);

    response = this.tgElasticSearchClient.metaData4Uris(allowedUris, response);

    if (!(allUris.size() > TGElasticSearchClient.getItemLimit())) {
      response = TGSearchUtils.addEmptyNodes(response, allUris, allowedUris);
    }

    return response;
  }

  @Override
  @GET
  @Path("/{uri}/parents")
  @Produces({"application/xml", "text/xml"})
  @Operation(
        summary = "parent aggregations",
        description = "parent aggregations for TextGrid Object",
        responses = {
            @ApiResponse(
                responseCode = "200",
                content = @Content(mediaType = "application/xml")
            )
        }
  )
  public TextgridUris getParents(@PathParam("uri") String uri) {

    TextgridUris parents = new TextgridUris();

    HashMap<String, Object> vars = new HashMap<String, Object>();
    vars.put("id", uri);
    System.out.println("uri is: " + uri);
    String query = TGSearchUtils.createQueryFromTemplate("getParents.tmpl", vars);

    log.info(query);

    try {
      InputStream in = this.sparqlClient.sparql(query);
      TreeSet<String> uris = TGSearchUtils.urisFromSparqlResponse(in, this.xmlInputFactory);
      uris.remove(uri);
      parents.getTextgridUri().addAll(uris);
    } catch (IOException e) {
      log.error("error in rdfstore request", e);
    }

    return parents;
  }

  @Override
  @GET
  @Path("/{uri}/children")
  @Produces({"application/xml", "text/xml"})
  @Operation(
        summary = "child aggregations",
        description = "child aggregations and objects for TextGrid aggregation",
        responses = {
            @ApiResponse(
                responseCode = "200",
                content = @Content(mediaType = "application/xml")
            )
        }
  )
  public TextgridUris getChildren(@PathParam("uri") String uri) {

    TextgridUris children = new TextgridUris();

    HashMap<String, Object> vars = new HashMap<String, Object>();
    vars.put("id", uri);
    System.out.println("uri is: " + uri);
    String query = TGSearchUtils.createQueryFromTemplate("getChildren.tmpl", vars);

    log.info(query);

    try {
      InputStream in = this.sparqlClient.sparql(query);
      TreeSet<String> uris = TGSearchUtils.urisFromSparqlResponse(in, this.xmlInputFactory);
      uris.remove(uri);
      children.getTextgridUri().addAll(uris);
    } catch (IOException e) {
      log.error("error in rdfstore request", e);
    }

    return children;
  }

  @Override
  @GET
  @Path("/{projectId}/allObjects")
  @Produces({"application/xml", "text/xml"})
  @Operation(
        summary = "all textgrid uris",
        description = "all textgrid uris for the objects contained in the project with id ‘projectId’",
        responses = {
            @ApiResponse(
                responseCode = "200",
                content = @Content(mediaType = "application/xml")
            )
        }
  )
  public TextgridUris getProjectMembers(
      @PathParam("projectId") String projectId,
      @QueryParam("start") @DefaultValue("0") int start,
      @QueryParam("limit") @DefaultValue("20") int limit) {
    TextgridUris children = new TextgridUris();

    SearchRequest searchRequest = new SearchRequest(this.tgElasticSearchClient.getEsIndex());

    TermQueryBuilder projectFilter = QueryBuilders.termQuery("project.id", projectId);

    BoolQueryBuilder projectQuery = QueryBuilders.boolQuery();
    projectQuery.must(projectFilter);

    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    String[] includeFields = new String[] {
        TGSearchImplConstants.TG_UNIQUE_IDENTIFIER_FIELD,
    };
    String[] excludeFields = Strings.EMPTY_ARRAY;

    searchSourceBuilder
      .fetchSource(includeFields, excludeFields)
      .from(start)
      .size(limit)
      .query(projectQuery);

    searchRequest.source(searchSourceBuilder);

    SearchResponse searchResponse = null;
    try {
      searchResponse = this.tgElasticSearchClient.getEsClient().search(searchRequest, RequestOptions.DEFAULT);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    children.setHits(Long.toString(searchResponse.getHits().getTotalHits().value));
    children.setLimit(Integer.toString(limit));
    children.setStart(Integer.toString(start));

    List<String> uris = new ArrayList<String>();
    for (SearchHit singleSearchHit : searchResponse.getHits()) {
      String tguri = singleSearchHit.getSourceAsMap()
          .get(TGSearchImplConstants.TG_UNIQUE_IDENTIFIER_FIELD).toString();
      uris.add(tguri);
    }

    children.getTextgridUri().addAll(uris);
    return children;
  }

}
