/*
 * #%L TextGrid :: TGSearch :: Core
 *
 * %% Copyright (C) 2011-2019 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen
 * (http://www.sub.uni-goettingen.de) %%
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L%
 */

package info.textgrid.middleware.tgsearch;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.TreeSet;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import javax.xml.stream.XMLInputFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import info.textgrid.middleware.tgsearch.api.Relation;
import info.textgrid.middleware.tgsearch.auth.AuthClient;
import info.textgrid.middleware.tgsearch.utils.TGSearchUtils;
import info.textgrid.namespaces.middleware.tgsearch.RelationResponseType;
import info.textgrid.namespaces.middleware.tgsearch.RelationType;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.utils.sesameclient.SesameClient;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

public class ElasticSearchRelationImpl implements Relation {

  private TGElasticSearchClient tgElasticSearchClient;
  private SesameClient sparqlClient;
  private AuthClient authClient;
  private Log log = LogFactory.getLog(ElasticSearchRelationImpl.class);
  private XMLInputFactory xmlInputFactory;

  public ElasticSearchRelationImpl(TGElasticSearchClient tgElasticSearchClient,
      SesameClient sparqlClient, AuthClient authClient) {
    this.tgElasticSearchClient = tgElasticSearchClient;
    this.sparqlClient = sparqlClient;
    this.authClient = authClient;
    this.xmlInputFactory = XMLInputFactory.newInstance();
  }

  /*
   * (non-Javadoc)
   *
   * @see info.textgrid.middleware.tgsearch.Relation#getRelation(java.lang.String, java.lang.String,
   * java.lang.String, java.lang.String)
   */
  @Override
  @GET
  @Path("/{relation}/{uri}/")
  @Produces("text/xml")
  @Operation(
        summary = "shows relations",
        description = "shows relations of type {relation} for the requested textgridUri",
        responses = {
            @ApiResponse(
                responseCode = "200",
                content = @Content(mediaType = "application/xml")
            )
        }
  )
  public Response getRelation(
      @QueryParam("sid") @DefaultValue("") final String sid,
      @QueryParam("log") @DefaultValue("") final String logstring,
      @PathParam("relation") final String relation,
      @PathParam("uri") final String uri) {

    Response response = new Response();
    try {
      treeQuery(relation, uri, response);
    } catch (IOException e) {
      // on error with sparql query return empty response
      // TODO: throw error to client
      log.error("error in rdfstore request", e);
      return response;
    }
    return response;
  }

  /*
   * (non-Javadoc)
   *
   * @see info.textgrid.middleware.tgsearch.Relation#getCombined(java.lang.String, java.lang.String,
   * java.lang.String, java.lang.String)
   */
  @Override
  @GET
  @Path("/{relation}/{uri}/combined")
  @Produces("text/xml")
  @Operation(
        summary = "shows relations",
        description = "shows relations of type {relation} for the requested textgridUri, and also the metadata, if sid authorized",
        responses = {
            @ApiResponse(
                responseCode = "200",
                content = @Content(mediaType = "application/xml")
            )
        }
  )
  public Response getCombined(
      @QueryParam("sid") @DefaultValue("") final String sid,
      @QueryParam("log") @DefaultValue("") final String logstring,
      @PathParam("relation") final String relation,
      @PathParam("uri") final String uri) {

    Response response = new Response();
    boolean needPathInfo = false;

    List<String> allUris;
    try {
      allUris = treeQuery(relation, uri, response);
    } catch (IOException e) {
      // on error with sparql query return empty response
      // TODO: throw error to client
      log.error("error in rdfstore request", e);
      return response;
    }

    List<String> allowedUris = authClient.filterUris(sid, allUris);

    this.tgElasticSearchClient.metaData4Uris(allowedUris, response, needPathInfo);

    response = TGSearchUtils.addEmptyNodes(response, allUris, allowedUris);

    return response;

  }


  /*
   * (non-Javadoc)
   *
   * @see info.textgrid.middleware.tgsearch.Relation#getOnlyMetadata(java.lang.String,
   * java.lang.String, java.lang.String, java.lang.String)
   */
  @Override
  @GET
  @Path("/{relation}/{uri}/metadata")
  @Produces("text/xml")
  @Operation(
        summary = "shows metadata",
        description = "shows metadata for objects which are related of type {relation} to the requested textgridUri",
        responses = {
            @ApiResponse(
                responseCode = "200",
                content = @Content(mediaType = "application/xml")
            )
        }
  )
  public Response getOnlyMetadata(
      @QueryParam("sid") @DefaultValue("") final String sid,
      @QueryParam("log") @DefaultValue("") final String logstring,
      @PathParam("relation") final String relation,
      @PathParam("uri") final String uri,
      @QueryParam("path") @DefaultValue("false") final String path) {

    Response response = new Response();
    boolean needPathInfo = Boolean.parseBoolean(path);

    List<String> allUris;
    try {
      allUris = treeQuery(relation, uri, new Response());
    } catch (IOException e) {
      // on error with sparql query return empty response
      // TODO: throw error to client
      log.error("error in rdfstore request", e);
      return new Response();
    }

    List<String> allowedUris = authClient.filterUris(sid, allUris);

    this.tgElasticSearchClient.metaData4Uris(allowedUris, response, needPathInfo);

    response = TGSearchUtils.addEmptyNodes(response, allUris, allowedUris);

    return response;

  }

  /**
   * retrieve network of related uris for a given uri from an RDF-Store via sparql append to
   * jaxb-response-element
   *
   * @param relation
   * @param uri
   * @throws IOException
   */
  protected List<String> treeQuery(String relation, String uri, Response res) throws IOException {

    RelationResponseType relationResponse = new RelationResponseType();

    List<String> foundUris = new ArrayList<String>();
    foundUris.add(uri); // uri is already known and asked for

    Queue<String> queue = new LinkedList<String>();
    queue.add(uri);

    while (!queue.isEmpty()) {

      String tguri = queue.poll();

      /*
       * look right
       */
      String query =
          "PREFIX tg:        <http://textgrid.info/relation-ns#> \n" +
              "\n" +
              "SELECT ?uri \n" +
              "WHERE { <" + tguri + "> tg:" + relation + " ?uri  }";

      InputStream in = sparqlClient.sparql(query);
      TreeSet<String> r1 = TGSearchUtils.urisFromSparqlResponse(in, this.xmlInputFactory);

      // 1.1 construct return-xml
      for (String tmp : r1) {
        // check if tmp already visited. if not, add to queue
        if (!foundUris.contains(tmp)) {
          relationResponse.getRelation().add(
              jaxbRelationEntry(tguri, relation, tmp));
          queue.add(tmp);
          foundUris.add(tmp);
        }
      }

      /*
       * look left
       */
      query =
          "PREFIX tg:        <http://textgrid.info/relation-ns#> \n" +
              "\n" +
              "SELECT ?uri \n" +
              "WHERE { ?uri tg:" + relation + " <" + tguri + ">  }";

      in = sparqlClient.sparql(query);
      r1 = TGSearchUtils.urisFromSparqlResponse(in, this.xmlInputFactory);

      for (String tmp : r1) {
        if (!foundUris.contains(tmp)) {
          relationResponse.getRelation().add(
              jaxbRelationEntry(tguri, relation, tmp));
          queue.add(tmp);
          foundUris.add(tmp);
        }
      }

    }

    res.setRelationResponse(relationResponse);
    return foundUris;

  }

  protected RelationType jaxbRelationEntry(String s, String p,
      String o) {

    RelationType rel = new RelationType();
    rel.setS(s);
    rel.setP(p);
    rel.setO(o);

    return rel;
  }

}
