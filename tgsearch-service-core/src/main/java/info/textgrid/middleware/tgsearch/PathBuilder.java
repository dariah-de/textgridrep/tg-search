/*
 * #%L TextGrid :: TGSearch :: Core
 *
 * %% Copyright (C) 2011-2019 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen
 * (http://www.sub.uni-goettingen.de) %%
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L%
 */

package info.textgrid.middleware.tgsearch;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.elasticsearch.action.get.MultiGetRequest;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.Strings;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import info.textgrid.namespaces.middleware.tgsearch.EntryType;
import info.textgrid.namespaces.middleware.tgsearch.PathGroupType;
import info.textgrid.namespaces.middleware.tgsearch.PathResponse;
import info.textgrid.namespaces.middleware.tgsearch.PathType;

public class PathBuilder {

  private TGElasticSearchClient tgEsClient;
  private Log log = LogFactory.getLog(PathBuilder.class);
  private String recLogFile = "/var/log/dhrep/tgsearch/recursive-aggregations.log";

  // TODO Path ID for more than one parent || oder flag
  public PathBuilder(TGElasticSearchClient tgEsClient) {
    this.tgEsClient = tgEsClient;
  }


  // Function for an entry in a path response containing the information about the textgridUri,
  // title and the format

  public EntryType pathEntry(String id) {

    EntryType textGridPathEntry = new EntryType();

    //MultiGetRequestBuilder path4Uri = tgEsClient.getEsClient().prepareMultiGet();
    MultiGetRequest path4Uri = new MultiGetRequest();
    String formatedId = id.replaceAll(TGSearchImplConstants.TG_URI_PREFIX, "");

    String[] includes = new String[] {"title", "format", "textgridUri"};
    String[] excludes = Strings.EMPTY_ARRAY;

    FetchSourceContext fetchSourceContext =
        new FetchSourceContext(true, includes, excludes);

    path4Uri.add(new MultiGetRequest.Item(tgEsClient.getEsIndex(),
        formatedId).fetchSourceContext(fetchSourceContext));

    MultiGetResponse path4allUris = null;
    try {
      path4allUris = this.tgEsClient.getEsClient().mget(path4Uri, RequestOptions.DEFAULT);
    } catch (IOException e) {
      log.error("error with multiget for path: ", e);
    }
    for (MultiGetItemResponse path4allUrisResp : path4allUris.getResponses()) {

      if (path4allUrisResp.getResponse().isExists()) {
        Map<String, Object> path4allUrisContent = path4allUrisResp.getResponse().getSourceAsMap();

        textGridPathEntry
            .setTextgridUri(path4allUrisContent.get("textgridUri").toString());
        textGridPathEntry.setTitle(path4allUrisContent.get("title").toString());
        textGridPathEntry.setFormat(path4allUrisContent.get("format").toString());
      }

    }

    return textGridPathEntry;

  }

  public void buildPathRecursive(PathType pt, String uri) {

    String uriTmp = formatedUri(uri);

    String query = " PREFIX ore:<http://www.openarchives.org/ore/terms/>"
        + "      PREFIX tg:<http://textgrid.info/relation-ns#>"
        + "      SELECT ?uri\n"
        + "      WHERE {\n"
        + "		 <textgrid:" + uriTmp
        + ">  (^ore:aggregates|^tg:isBaseUriOf/^ore:aggregates) ?uri\n"
        + "      } GROUP BY ?uri";

    List<String> uris = new ArrayList<String>(tgEsClient.getUrisFromQuery(query));

    if (!(uris.isEmpty()) && !(uris.get(0).isEmpty())) {
      // break recursion, if incoming uri is found in result set
      if (!uris.get(0).equals(uri)) {
        buildPathRecursive(pt, uris.get(0));
        EntryType et = pathEntry(uris.get(0));
        pt.getEntry().add(et);
      } else {
        try {
          String recLogEntry = uri + " is an aggregation containing itself\n";
          FileUtils.writeStringToFile(new File(recLogFile), "UTF-8", recLogEntry, true);
        } catch (IOException e) {
          log.error("could not write to " + recLogFile, e);
        }
      }
    }

  }

  // Building the basic structure of the path response. Just missing the "Entry" tag

  // <ns4:pathResponse xmlns:tg="http://textgrid.info/relation-ns#"
  // xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  // xmlns="http://textgrid.info/namespaces/metadata/core/2010"
  // xmlns:ns4="http://www.textgrid.info/namespaces/middleware/tgsearch">
  // <ns4:pathGroup>
  // <ns4:path>
  //
  // </ns4:path>
  // </ns4:pathGroup>
  // </ns4:pathResponse>

  public PathResponse listPath(String uri) {

    String uriTmp = formatedUri(uri);

    PathResponse textgridRepPath = new PathResponse();
    PathGroupType textgridRepPathGroup = new PathGroupType();
    PathType pt = new PathType();
    textgridRepPathGroup.setStartUri("textgrid:" + uriTmp);

    buildPathRecursive(pt, uriTmp);

    textgridRepPathGroup.getPath().add(pt);
    textgridRepPath.getPathGroup().add(textgridRepPathGroup);

    return textgridRepPath;

  }

  public static String formatedUri(String uri) {

    String uriTmp = uri;

    if (uri.startsWith("textgrid:")) {
      uriTmp = uri.substring(TGSearchImplConstants.TG_URI_PREFIX_LENGTH);
    }

    return uriTmp;
  }

}
