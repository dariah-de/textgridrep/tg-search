/*
 * #%L TextGrid :: TGSearch :: Core
 *
 * %% Copyright (C) 2011-2019 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen
 * (http://www.sub.uni-goettingen.de) %%
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L%
 */

package info.textgrid.middleware.tgsearch;

import java.io.IOException;
import java.util.List;
import java.util.Map.Entry;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.BucketOrder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import info.textgrid.middleware.tgsearch.api.FacetQuery;
import info.textgrid.middleware.tgsearch.auth.AuthClient;
import info.textgrid.middleware.tgsearch.utils.ElasticSearchFilters;
import info.textgrid.namespaces.middleware.tgsearch.FacetGroupType;
import info.textgrid.namespaces.middleware.tgsearch.FacetResponse;
import info.textgrid.namespaces.middleware.tgsearch.FacetType;

public class ElasticSearchFacetImpl implements FacetQuery {

  private AuthClient authClient;
  private TGElasticSearchClient tgEsclient;

  private Log log = LogFactory.getLog(ElasticSearchFacetImpl.class);

  public static final int MAX_FACETS = 10000;

  public ElasticSearchFacetImpl(AuthClient authClient, TGElasticSearchClient tgesClient) {
    this.authClient = authClient;
    this.tgEsclient = tgesClient;
  }


  /**
   * TODO: querystring, filter, sid for tgauthfilter datetime todo: years need to be in colons in
   * json, otherwise not recognized
   *
   */
  @Override
  @GET
  @Path("/")
  @Produces({"application/xml", "text/xml"})
  public FacetResponse facetQuery(
      @QueryParam("facet") List<String> facetList,
      @QueryParam("limit") @DefaultValue("10") int limit,
      @QueryParam("order") @DefaultValue("count:desc") String order,
      @QueryParam("sid") String sid,
      @QueryParam("sandbox") @DefaultValue("false") final boolean sandbox){

    FacetResponse response = new FacetResponse();

    SearchRequest searchRequest = new SearchRequest(this.tgEsclient.getEsIndex());
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

    boolean filterProjects4Auth = false;
    TermsQueryBuilder projectFilter = null;
    if (authClient.isProjectAuth()) {
      log.debug("query auth for searchable projects");
      // get list of projects browseable by user
      List<String> projectsAllowedToSearch = authClient
          .getProjectsForSID(sid);
      projectFilter = QueryBuilders.termsQuery("project.id", projectsAllowedToSearch);
      filterProjects4Auth = true;
    }

    if(limit == 0) {
      limit = MAX_FACETS;
    }

    /**
     * default to count/desc or term/asc, if only "count" or "term" given
     */
    BucketOrder bucketOrder;
    String[] orderArr = order.split(":");
    if (orderArr[0].equalsIgnoreCase("term")) {
      boolean asc = true;
      if (orderArr.length > 1) {
        asc = !orderArr[1].equalsIgnoreCase("desc");
      }
      bucketOrder = BucketOrder.key(asc);
    } else {
      boolean asc = false;
      if (orderArr.length > 1) {
        asc = orderArr[1].equalsIgnoreCase("asc");
      }
      bucketOrder = BucketOrder.count(asc);
    }

    for (String facet : facetList) {
      if (!facet.endsWith(".untouched") &! this.tgEsclient.getUntouchedFieldExceptions().contains(facet)) {
        facet += ".untouched";
      }
      AggregationBuilder agg = AggregationBuilders
          .terms(facet)
          .field(facet)
          .order(bucketOrder)
          .size(limit);
      searchSourceBuilder.aggregation(agg);
    }

    BoolQueryBuilder queryfilter = QueryBuilders.boolQuery();
    // don't show hits from sandbox if not requested, makes only sense for public tgsearch
    if (!authClient.isProjectAuth() && !sandbox) {
      queryfilter.mustNot(QueryBuilders.existsQuery("nearlyPublished"));
    }
    if (filterProjects4Auth) {
      queryfilter.must(projectFilter);
    }
    searchSourceBuilder.query(queryfilter);

    searchSourceBuilder.size(0);
    searchRequest.source(searchSourceBuilder);
    //System.out.println(Strings.toString(searchSourceBuilder, true, true));
    SearchResponse searchResponse = null;
    try {
      searchResponse = this.tgEsclient.getEsClient().search(searchRequest, RequestOptions.DEFAULT);
    } catch (IOException e) {
      log.error(e);
    }

    for (Entry<String, Aggregation> entry : searchResponse.getAggregations().asMap().entrySet()) {

      FacetGroupType facetGroup = new FacetGroupType();
      String name = entry.getKey();
      if (name.endsWith(".untouched")) {
        name = name.substring(0, name.lastIndexOf("."));
      }
      facetGroup.setName(name);

      Terms a = (Terms) entry.getValue();
      for (Bucket bentry : a.getBuckets()) {
        FacetType facet = new FacetType();
        facet.setCount(bentry.getDocCount());
        facet.setValue(bentry.getKey().toString());
        facetGroup.getFacet().add(facet);
      }
      response.getFacetGroup().add(facetGroup);
    }

    return response;
  }
}
