/*
 * #%L TextGrid :: TGSearch :: Core
 *
 * %% Copyright (C) 2011-2019 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen
 * (http://www.sub.uni-goettingen.de) %%
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L%
 */

package info.textgrid.middleware.tgsearch;

import java.util.List;
import java.util.Arrays;

public final class TGSearchImplConstants {

  private TGSearchImplConstants() {
    // not called
  }

  public static final String ES_FULLTEXT_FIELD = "fulltext";
  public static final String ES_ORIGINAL_METADATA_FIELD = "original_metadata";
  public static final String ES_ORIGINAL_DATA_FIELD = "original_data";
  // public static final int ITEM_LIMIT = 2500;
  public static final int TG_URI_PREFIX_LENGTH = 9;
  public static final int URI_LIMIT_AUTH = 500;
  public static final int STRING_TO_HIGHLIGHT_SIZE = 3;
  public static final int STRING_TO_HIGHLIGHT_STATEMENT_LENGTH_OPEN = 4;
  public static final int STRING_TO_HIGHLIGHT_STATEMENT_LENGTH_CLOSE = 5;
  public static final String TG_URI_PREFIX = "textgrid:";
  public static final String TG_UNIQUE_IDENTIFIER_FIELD = "textgridUri";
  public static final String TG_SANDBOX_FIELD = "nearlyPublished";

}
