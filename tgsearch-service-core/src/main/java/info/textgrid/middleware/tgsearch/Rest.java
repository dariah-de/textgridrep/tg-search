/*
 * #%L TextGrid :: TGSearch :: Core
 *
 * %% Copyright (C) 2011-2019 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen
 * (http://www.sub.uni-goettingen.de) %%
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L%
 */

package info.textgrid.middleware.tgsearch;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class Rest {

  @Autowired
  private TGElasticSearchClient tgEsclient;

  private Log log = LogFactory.getLog(Rest.class);

  @GET
  @Path("{a:getVersion|version}")
  @Produces("text/html")
  public String getVersion() {

    InputStream is = this.getClass().getResourceAsStream("buildinfo.txt");
    Properties prop = new Properties();
    try {
      prop.load(is);
    } catch (IOException e) {
      return "Error opening buildinfo.txt";
    } finally {
      try {
        is.close();
      } catch (IOException e) {
        log.error("error closing stream", e);
      }
    }

    String info = "This is tgsearch " + prop.get("version")
        + " built " + prop.get("time") + " from: "
        + prop.get("branch") + " (rev:" + prop.get("build") + ")";

    return info;
  }

  @GET
  @Path("config")
  @Produces("text/plain")
  public String getConfig() {

    String res = "UntouchedFieldExceptions: " + String.join (", ", this.tgEsclient.getUntouchedFieldExceptions()) + "\n";
    res += "ItemLimit: " + TGElasticSearchClient.getItemLimit() + "\n";
    return res;
  }

}
