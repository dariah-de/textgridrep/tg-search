.. tgsearch documentation master file, created by
   sphinx-quickstart on Wed May 20 12:51:17 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

TG-search
=========

TG-search is the search service for the textgrid repository. There are **tgsearch-public** and **tgsearch-nonpublic**. tgsearch-public is for searching published data in the textgrid repository and does not require authentication (a sessionId). tgsearch-nonpublic is for searching the not public data the user has access to, this requires a sessionId.


Configuration
-------------

TG-search is configured and setup by puppet.


Dependencies
------------

* ElasticSearch
* Triple Store, see triplestore_documentation_
* TG-auth, see tgauth_documentation_


Endpoints
---------

- Production server
    - tgsearch-public: https://textgridlab.org/1.0/tgsearch-public/
    - tgsearch-nonpublic: https://textgridlab.org/1.0/tgsearch/

- Development server
    - tgsearch-public: https://dev.textgridlab.org/1.0/tgsearch-public/
    - tgsearch-nonpublic: https://dev.textgridlab.org/1.0/tgsearch/


API Documentation
------------------

.. toctree::
   :maxdepth: 2

   api/search.rst
   api/info.rst
   api/navigation.rst
   api/facet.rst
   api/relation.rst

Sources
-------
See tgsearch_sources_

License
-------

See LICENCE_

.. _LICENCE: https://gitlab.gwdg.de/dariah-de/textgridrep/tg-search/-/blob/main/LICENSE.txt
.. _triplestore_documentation: https://textgridlab.org/doc/services/triplestore.html
.. _tgauth_documentation: https://textgridlab.org/doc/services/submodules/tg-auth/docs/index.html
.. _tgsearch_sources: https://gitlab.gwdg.de/dariah-de/textgridrep/tg-search/-/tree/main
