GET /search
===========

Request Parameters:

==========================  ======= ========= ======================================================================
Parameter                   Type    Default   Description
==========================  ======= ========= ======================================================================
q                           String            Lucene search String
sid                         String            TextGrid SessionID from tgauth
target                      String  both      where to do fulltext-searches: 
                                              one of "structure", "metadata" and "both"
order                       String  relevance key-value ascending (asc) or descending (desc) and 
                                              metadata-field like asc:title or desc:author
start                       Integer 0         result number to start with.
limit                       Integer 20        number of entries to return.
kwicWidth                   Integer 40        number of chars before and after a kwic match
wordDistance                Integer -1        max distance beetween two words in fulltext query. ignored if set 
                                              to a number < 0, then for a hit all words must be contained in one document.
path                        Boolean false     path of found result(work->edition->aggregations) should be applied to hit
allProjects                 Boolean false     all Projects should be searched for public data, 
                                              warning: this query may be slow, if many results found
sandbox                     Boolean false     show sandboxed (not yet finally published) data
filter                      String            add filter on query results, e.g. for faceting (TODO: Syntax)
facet                       String            get facets for query results
facetlimit                  Integer 10        number of results to return for each facet
==========================  ======= ========= ======================================================================

Response:

    List of TextGrid objects found. XML, using the textgrid medataschema

Example request::

    curl https://textgridlab.org/1.0/tgsearch-public/search?q=waldeinsamkeit

Example response::

    <tgs:response hits="84" session="deprecated" start="0" limit="20">
      <tgs:result>
        <object>
          <generic>
              <provided>
                <title>Waldeinsamkeit</title><format>text/tg.work+xml</format>
              </provided>
              <generated>
                <created>2012-01-29T23:35:36.930+01:00</created>
                <lastModified>2012-01-29T23:35:36.930+01:00</lastModified>
                <issued>2012-01-29T23:35:36.930+01:00</issued>
                <textgridUri extRef="">textgrid:rnqb.0</textgridUri>
                <revision>0</revision>
                <pid pidType="handle">hdl:11858/00-1734-0000-0003-EFDA-1</pid>
                <extent>38</extent>
                <dataContributor>tvitt@textgrid.de</dataContributor>
                <project id="TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c">Digitale Bibliothek</project>
                <availability>public</availability>
              </generated>
          </generic>
          <work>
            <agent role="author" id="pnd:119002248">Lingg, Hermann von</agent>
            <dateOfCreation notBefore="1820" notAfter="1905"></dateOfCreation>
            <genre>verse</genre>
          </work>
        </object>
      </tgs:result>
      [...] 
    </tgs:response>


Search syntax
-------------
The search query passed with the parameter "q" can be written in lucene syntax, as described in lucene_syntax_


Facets and filters
------------------

For facetting the search result it is possible to generate facets on metadata fields. 
These facets are generated on the whole set of objetcs matching the current search request. 
The parameter facet is repeatable.

Example request (facets on format and agent)::

    curl "https://textgridlab.org/1.0/tgsearch-public/search?q=waldeinsamkeit&facet=format&facet=edition.agent.value"

Example response::


    <tgs:response hits="85" session="deprecated" start="0" limit="20">
      <tgs:result>
        <object>
          <generic>
            <provided>
              <title>Waldeinsamkeit</title><format>text/tg.work+xml</format>
            </provided>
            <generated>
              <created>2012-01-29T23:35:36.930+01:00</created>
              <lastModified>2012-01-29T23:35:36.930+01:00</lastModified>
              <issued>2012-01-29T23:35:36.930+01:00</issued>
              <textgridUri extRef="">textgrid:rnqb.0</textgridUri>
              <revision>0</revision>
              <pid pidType="handle">hdl:11858/00-1734-0000-0003-EFDA-1</pid>
              <extent>38</extent>
              <dataContributor>tvitt@textgrid.de</dataContributor>
              <project id="TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c">Digitale Bibliothek</project>
              <availability>public</availability>
            </generated>
          </generic>
          <work>
            <agent role="author" id="pnd:119002248">Lingg, Hermann von</agent>
            <dateOfCreation notBefore="1820" notAfter="1905"></dateOfCreation>
            <genre>verse</genre>
          </work>
        </object>
      </tgs:result>

      [...]

      <tgs:facetResponse>
        <tgs:facetGroup name="format">
          <tgs:facet count="79">text/xml</tgs:facet>
          <tgs:facet count="3">text/tg.edition+tg.aggregation+xml</tgs:facet>
          <tgs:facet count="3">text/tg.work+xml</tgs:facet>
        </tgs:facetGroup>
        <tgs:facetGroup name="edition.agent.value">
          <tgs:facet count="9">Eichendorff, Joseph von</tgs:facet>
          <tgs:facet count="4">Bechstein, Ludwig</tgs:facet>
          <tgs:facet count="4">Kerner, Justinus</tgs:facet>
          <tgs:facet count="3">Schöppner, Alexander</tgs:facet>
          <tgs:facet count="2">Arnim, Ludwig Achim von</tgs:facet>
          <tgs:facet count="2">Geibel, Emanuel</tgs:facet>
          <tgs:facet count="2">Grässe, Johann Georg Theodor</tgs:facet>
          <tgs:facet count="2">Gutzkow, Karl</tgs:facet>
          <tgs:facet count="2">Heine, Heinrich</tgs:facet>
          <tgs:facet count="2">Lingg, Hermann von</tgs:facet>
        </tgs:facetGroup>
      </tgs:facetResponse>
    </tgs:response>

based on this facets it is possible to apply filter, e.g. all files with format "text/xml" where the agent is "Eichendorff, Joseph von".

Example request (filter for xml from agent Eichendorff)::

    curl "https://textgridlab.org/1.0/tgsearch-public/search?q=waldeinsamkeit&filter=format:text/xml&filter=edition.agent.value:Eichendorff,%20Joseph%20von"

Example request (all image/jpeg files from the project "Digitale Bibliothek")::

    curl "https://textgridlab.org/1.0/tgsearch-public/search/?filter=format:image/jpeg&filter=project.id:TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c"



.. _lucene_syntax: https://textgridrep.org/en/syntax

