GET /navigation/{projectID}
===========================

Request Parameters

==========================  ======= ======================================================================
Parameter                   Type    Description
==========================  ======= ======================================================================
sid                         String  TextGrid SessionID
==========================  ======= ======================================================================

Response

	 objects belonging to a project, filter objects which are in an aggregation in same project.

Example request::

    curl https://textgridlab.org/1.0/tgsearch-public/navigation/TGPR-26236625-1acc-b921-a5fa-53567c3eeb80

GET /navigation/agg/{textgridUri}
=================================

Request Parameters

==========================  ======= ======================================================================
Parameter                   Type    Description
==========================  ======= ======================================================================
sid                         String  TextGrid SessionID
==========================  ======= ======================================================================

Response

	 objects belonging to an aggregation.

Example request::

    curl https://textgridlab.org/1.0/tgsearch-public/navigation/agg/textgrid:22g82.0

GET /navigation/toplevel
========================

Request Parameters

==========================  ======= ========= ======================================================================
Parameter                   Type    Default   Description
==========================  ======= ========= ======================================================================
sandbox                     Boolean false     show sandboxed (not yet finally published) data
==========================  ======= ========= ======================================================================

Response

    all toplevel objects, use for listing public repository, returns empty result on nonpublic instances

Example request::

    curl https://textgridlab.org/1.0/tgsearch-public/navigation/toplevel

