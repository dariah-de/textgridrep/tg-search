GET /info/{textgridUri}
=======================

Request Parameters

==========================  ======= ======================================================================
Parameter                   Type    Description
==========================  ======= ======================================================================
sid                         String  TextGrid SessionID
==========================  ======= ======================================================================

Response

    tgsearch response with metadata for single TextGrid object

Example request::

    curl https://textgridlab.org/1.0/tgsearch-public/info/textgrid:mrhg.0

Example response::

    <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <tgs:response>
        <tgs:result textgridUri="textgrid:mrhg.0">
            <object>
                <generic>
                    <provided>
                        <title>Die Werber</title><format>text/xml</format>
                    </provided>
                    <generated>
                        <created>2012-01-11T02:15:23.681+01:00</created>
                        <lastModified>2012-01-11T02:15:23.681+01:00</lastModified>
                        <issued>2012-01-11T02:15:23.681+01:00</issued>
                        <textgridUri extRef="">textgrid:mrhg.0</textgridUri>
                        <revision>0</revision>
                        <pid pidType="handle">hdl:11858/00-1734-0000-0002-9861-3</pid>
                        <extent>8759</extent>
                        <dataContributor>tvitt@textgrid.de</dataContributor>
                        <project id="TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c">Digitale Bibliothek</project>
                        <availability>public</availability>
                    </generated>
                </generic>
                <item>
                    <rightsHolder id="">TextGrid</rightsHolder>
                </item>
            </object>
        </tgs:result>
    </tgs:response>


GET /info/{textgridUri}/revisions
=================================

Request Parameters

==========================  ======= ======================================================================
Parameter                   Type    Description
==========================  ======= ======================================================================
sid                         String  TextGrid SessionID
==========================  ======= ======================================================================

Response

    list of revisions for a single TextGrid object

Example request::

    curl https://textgridlab.org/1.0/tgsearch-public/info/textgrid:mrhg.0/revisions

Example response::

    <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <tgs:revisions textgridUri="textgrid:mrhg">
        <tgs:revision>0</tgs:revision>
    </tgs:revisions>


GET /info/{textgridUri}/revisions/meta
======================================

Request Parameters

==========================  ======= ======================================================================
Parameter                   Type    Description
==========================  ======= ======================================================================
sid                         String  TextGrid SessionID
==========================  ======= ======================================================================

Response

    list of revisions and their metadata for a single TextGrid object

Example request::

    curl https://textgridlab.org/1.0/tgsearch-public/info/textgrid:mrhg.0/revisions/meta

Example response::

    <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <tgs:response>
            <object>
                <generic>
                    <provided>
                        <title>Die Werber</title><format>text/xml</format>
                    </provided>
                    <generated>
                        <created>2012-01-11T02:15:23.681+01:00</created>
                        <lastModified>2012-01-11T02:15:23.681+01:00</lastModified>
                        <issued>2012-01-11T02:15:23.681+01:00</issued>
                        <textgridUri extRef="">textgrid:mrhg.0</textgridUri>
                        <revision>0</revision>
                        <pid pidType="handle">hdl:11858/00-1734-0000-0002-9861-3</pid>
                        <extent>8759</extent>
                        <dataContributor>tvitt@textgrid.de</dataContributor>
                        <project id="TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c">Digitale Bibliothek</project>
                        <availability>public</availability>
                    </generated>
                </generic>
                <item>
                    <rightsHolder id="">TextGrid</rightsHolder>
                </item>
            </object>
        </tgs:result>
    </tgs:response>


GET /info/{textgridUri}/path
============================

Request Parameters

==========================  ======= ======================================================================
Parameter                   Type    Description
==========================  ======= ======================================================================
sid                         String  TextGrid SessionID
==========================  ======= ======================================================================

Response

    list path (edition->work->aggragation,...) for single TextGrid Object

Example request::

    curl https://textgridlab.org/1.0/tgsearch-public/info/textgrid:mrhg.0/path

Example response::

    <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <tgs:pathResponse>
        <tgs:pathGroup startUri="textgrid:mrhg.0">
            <tgs:path>
                <tgs:entry>
                    <tgs:textgridUri>textgrid:mr25.0</tgs:textgridUri>
                    <tgs:format>text/tg.collection+tg.aggregation+xml</tgs:format>
                    <tgs:title>Eichendorff, Joseph von</tgs:title>
                </tgs:entry>
                <tgs:entry>
                    <tgs:textgridUri>textgrid:ms15.0</tgs:textgridUri>
                    <tgs:format>text/tg.aggregation+xml</tgs:format>
                    <tgs:title>Gedichte</tgs:title>
                </tgs:entry>
                <tgs:entry>
                    <tgs:textgridUri>textgrid:ms02.0</tgs:textgridUri>
                    <tgs:format>text/tg.edition+tg.aggregation+xml</tgs:format>
                    <tgs:title>Gedichte (Ausgabe 1841)</tgs:title>
                </tgs:entry>
                <tgs:entry>
                    <tgs:textgridUri>textgrid:mr2d.0</tgs:textgridUri>
                    <tgs:format>text/tg.aggregation+xml</tgs:format>
                    <tgs:title>2. Sängerleben</tgs:title>
                </tgs:entry>
                <tgs:entry>
                    <tgs:textgridUri>textgrid:mrhh.0</tgs:textgridUri>
                    <tgs:format>text/tg.edition+tg.aggregation+xml</tgs:format>
                    <tgs:title>Die Werber</tgs:title>
                </tgs:entry>
            </tgs:path>
        </tgs:pathGroup>
    </tgs:pathResponse>


GET /info/{textgridUri}/editionWorkMeta
=======================================

Request Parameters

==========================  ======= ======================================================================
Parameter                   Type    Description
==========================  ======= ======================================================================
sid                         String  TextGrid SessionID
==========================  ======= ======================================================================

Response

    Edition and work metadata for given object from first matching parent edition

Example request::

    curl https://textgridlab.org/1.0/tgsearch-public/info/textgrid:mrhg.0/editionWorkMeta


GET /info/{textgridUri}/metadata
================================

Request Parameters

==========================  ======= ======================================================================
Parameter                   Type    Description
==========================  ======= ======================================================================
sid                         String  TextGrid SessionID
==========================  ======= ======================================================================

Response

    only the textgrid metatada object for given uri, not surrounded by tgsearch-response xml

Example request::

    curl https://textgridlab.org/1.0/tgsearch-public/info/textgrid:mrhg.0/metadata

Example response::

    <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <object>
        <generic>
            <provided>
                <title>Die Werber</title><format>text/xml</format>
            </provided>
            <generated>
                <created>2012-01-11T02:15:23.681+01:00</created>
                <lastModified>2012-01-11T02:15:23.681+01:00</lastModified>
                <issued>2012-01-11T02:15:23.681+01:00</issued>
                <textgridUri extRef="">textgrid:mrhg.0</textgridUri>
                <revision>0</revision>
                <pid pidType="handle">hdl:11858/00-1734-0000-0002-9861-3</pid>
                <extent>8759</extent>
                <dataContributor>tvitt@textgrid.de</dataContributor>
                <project id="TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c">Digitale Bibliothek</project>
                <availability>public</availability>
            </generated>
        </generic>
        <item>
            <rightsHolder id="">TextGrid</rightsHolder>
        </item>
    </object>


GET /info/{textgridUri}/parents
===============================

Response

    parent aggregations for TextGrid object

Example request::

    curl https://textgridlab.org/1.0/tgsearch-public/info/textgrid:mrhg.0/parents

Example response::

    <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <tgs:textgridUris>
        <tgs:textgridUri>textgrid:mrhh.0</tgs:textgridUri>
        <tgs:textgridUri>textgrid:mr2d.0</tgs:textgridUri>
        <tgs:textgridUri>textgrid:ms02.0</tgs:textgridUri>
        <tgs:textgridUri>textgrid:ms15.0</tgs:textgridUri>
        <tgs:textgridUri>textgrid:mr25.0</tgs:textgridUri>
    </tgs:textgridUris>


GET /info/{textgridUri}/children
================================

Response

    child aggregations and objects for TextGrid aggregation

Example request::

    curl https://textgridlab.org/1.0/tgsearch-public/info/textgrid:mrhg.0/children

GET /info/{projectId}/allObjects
================================

Request Parameters:

==========================  ======= ======================================================================
Parameter                   Type    Description
==========================  ======= ======================================================================
start                       Integer result number to start with
limit                       Integer number of entries to return
==========================  ======= ======================================================================

Response

    all textgrid uris for the objects contained in the project with id 'projectId'

Example request::

    curl https://textgridlab.org/1.0/tgsearch-public/info/TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c/allObjects?limit=100

