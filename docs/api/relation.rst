GET /relation/{relation}/{textgridUri}
======================================

Request Parameters

==========================  ======= ======================================================================
Parameter                   Type    Description
==========================  ======= ======================================================================
sid                         String  TextGrid SessionID
==========================  ======= ======================================================================


Response

	 shows relations of type {relation} for the requested textgridUri

Example request::

    curl http://textgrid-esx1.gwdg.de/1.0/tgsearch/relation/isDerivedFrom/textgrid:2024t.0

GET /relation/{relation}/{textgridUri}/combined
===============================================

Request Parameters

==========================  ======= ======================================================================
Parameter                   Type    Description
==========================  ======= ======================================================================
sid                         String  TextGrid SessionID
==========================  ======= ======================================================================


Response

	 shows relations of type {relation} for the requested textgridUri, and also the metadata, if sid authorized

Example request::

    curl http://textgrid-esx1.gwdg.de/1.0/tgsearch/relation/isDerivedFrom/textgrid:2024t.0/combined

GET /relation/{relation}/{textgridUri}/metadata
===============================================

Request Parameters

==========================  ======= ======================================================================
Parameter                   Type    Description
==========================  ======= ======================================================================
sid                         String  TextGrid SessionID
==========================  ======= ======================================================================


Response

	 shows metadata for objects which are related of type {relation} to the requested textgridUri

Example request::

    curl http://textgrid-esx1.gwdg.de/1.0/tgsearch/relation/isDerivedFrom/textgrid:2024t.0/metadata


