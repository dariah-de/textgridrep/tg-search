GET /facet
===========================

Request Parameters

==========================  ======= ========== ======================================================================
Parameter                   Type    Default    Description
==========================  ======= ========== ======================================================================
facetList                   String             metadata fields to create facets
limit                       Integer            number of entries per facet, defaults to 10, 0 for all entries
order                       String  count:desc order by count or term ascending (asc) or descending, (desc) e.g. setting the string "term:asc".
sid                         String             TextGrid SessionID
sandbox                     boolean false      show sandboxed (not yet finally published) data
==========================  ======= ========== ======================================================================

Response

	 objects belonging to a project, filter objects which are in an aggregation in same project.

Example request::

    curl "http://textgrid-esx2.gwdg.de/1.0/tgsearch-public/facet?facet=edition.agent.value&limit=0&order=term"

Example response::

    <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <tgs:facetResponse>
        <tgs:facetGroup name="edition.agent.value">
            <tgs:facet count="3">Abraham a Sancta Clara</tgs:facet>
            <tgs:facet count="861">Abschatz, Hans Aßmann von</tgs:facet>
            <tgs:facet count="78">Ahlefeld, Charlotte von</tgs:facet>
            <tgs:facet count="3">Aischylos</tgs:facet><tgs:facet count="3">Alberti, Konrad</tgs:facet>
            <tgs:facet count="3">Alexis, Willibald</tgs:facet>
            [...]
    </tgs:facetResponse>

Example request::

    curl "http://textgrid-esx2.gwdg.de/1.0/tgsearch-public/facet?facet=work.genre&facet=edition.agent.value&facet=format&facet=project.value&limit=3"

Example response::

    <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <tgs:facetResponse>
        <tgs:facetGroup name="edition.agent.value">
            <tgs:facet count="3596">Logau, Friedrich von</tgs:facet>
            <tgs:facet count="3493">Bartsch, Karl</tgs:facet>
            <tgs:facet count="3315">Grässe, Johann Georg Theodor</tgs:facet>
        </tgs:facetGroup>
        <tgs:facetGroup name="work.genre">
            <tgs:facet count="60048">verse</tgs:facet>
            <tgs:facet count="30335">other</tgs:facet>
            <tgs:facet count="3493">prose</tgs:facet>
        </tgs:facetGroup>
        <tgs:facetGroup name="format">
            <tgs:facet count="271857">image/jpeg</tgs:facet>
            <tgs:facet count="107069">text/xml</tgs:facet>
            <tgs:facet count="94652">text/tg.work+xml</tgs:facet>
        </tgs:facetGroup>
        <tgs:facetGroup name="project.value">
            <tgs:facet count="390586">St. Matthias Test 07</tgs:facet>
            <tgs:facet count="308715">Digitale Bibliothek</tgs:facet>
            <tgs:facet count="1825">Bonaventura - Nachtwachen</tgs:facet>
        </tgs:facetGroup>
    </tgs:facetResponse>


