/*
 * #%L
 * TextGrid :: TGSearch :: API
 * %%
 * Copyright (C) 2011 - 2012 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen (http://www.sub.uni-goettingen.de)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package info.textgrid.middleware.tgsearch.api;

import info.textgrid.namespaces.middleware.tgsearch.Response;

import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;

public interface Relation {

	@GET
	@Path("/{relation}/{uri}/")
	@Produces("text/xml")
	Response getRelation(
			@QueryParam("sid") @DefaultValue("") final String sid,
			@QueryParam("log") @DefaultValue("") final String logstring,
			@PathParam("relation") final String relation,
			@PathParam("uri") final String uri);

	@GET
	@Path("/{relation}/{uri}/combined")
	@Produces("text/xml")
	Response getCombined(
			@QueryParam("sid") @DefaultValue("") final String sid,
			@QueryParam("log") @DefaultValue("") final String logstring,
			@PathParam("relation") final String relation,
			@PathParam("uri") final String uri);

	@GET
	@Path("/{relation}/{uri}/metadata")
	@Produces("text/xml")
	Response getOnlyMetadata(
			@QueryParam("sid") @DefaultValue("") final String sid,
			@QueryParam("log") @DefaultValue("") final String logstring,
			@PathParam("relation") final String relation,
			@PathParam("uri") final String uri,
	        @QueryParam("path") @DefaultValue("false") final String path);
}