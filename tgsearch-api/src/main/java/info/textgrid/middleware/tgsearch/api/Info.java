/*
 * #%L
 * TextGrid :: TGSearch :: API
 * %%
 * Copyright (C) 2011 - 2012 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen (http://www.sub.uni-goettingen.de)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package info.textgrid.middleware.tgsearch.api;

import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgsearch.PathResponse;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.Revisions;
import info.textgrid.namespaces.middleware.tgsearch.TextgridUris;

import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.StreamingOutput;

public interface Info {

	@GET
	@Path("/{uri}")
	@Produces({ "application/xml", "text/xml" })
	Response metadata(@PathParam("uri") String uri,
			@QueryParam("sid") String sid, @QueryParam("path") @DefaultValue("false") String path);

	@GET
	@Path("/{uri}/revisions")
	@Produces({ "application/xml", "text/xml" })
	Revisions revisions(@PathParam("uri") String uri);
	
	@GET
	@Path("/{uri}/revisions/meta")
	@Produces({ "application/xml", "text/xml" })
	Response revisionsAndMeta(
			@PathParam("uri") String uri,
			@QueryParam("sid") String sid);

	@GET
	@Path("/{uri}/relations")
	@Produces({ "application/xml", "text/xml"})
	Response relations(@PathParam("uri") String uri);

	@GET
	@Path("/{uri}/baseline")
	@Produces({ "application/xml", "text/xml"})
	StreamingOutput baseline(@PathParam("uri") String uri, @QueryParam("sid") String sid);
	
	@GET
	@Path("/{uri}/path")
	@Produces({"application/xml", "text/xml"})
	PathResponse getPath(@PathParam("uri") String uri);
	
	/**
	 * retrieve metadata for given uri, and also the edition and work it belongs to
	 * @param uri
	 * @param sid
	 * 
	 */
	@GET
	@Path("/{uri}/editionWorkMeta")
	@Produces({"appliation/xml", "text/xml"})
	Response getEditionWorkMeta(@PathParam("uri") String uri, @QueryParam("sid") String sid);
	
	@GET
	@Path("/{uri}/metadata")
	@Produces({"application/xml", "text/xml"})
	ObjectType onlyMetadata(@PathParam("uri") String uri, @QueryParam("sid") String sid);
	
	@GET
	@Path("/{uri}/parents")
	@Produces({"application/xml", "text/xml"})
	TextgridUris getParents(@PathParam("uri") String uri);

	@GET
	@Path("/{uri}/children")
	@Produces({"application/xml", "text/xml"})
	TextgridUris getChildren(@PathParam("uri") String uri);
	
	@GET
	@Path("/{projectId}/allObjects")
	@Produces({"application/xml", "text/xml"})
	TextgridUris getProjectMembers(@PathParam("projectId") String projectId, @QueryParam("start") @DefaultValue("0") int start, @QueryParam("limit") @DefaultValue("20") int limit);
	
}