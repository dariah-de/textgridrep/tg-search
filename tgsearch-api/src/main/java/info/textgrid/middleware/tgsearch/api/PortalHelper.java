/*
 * #%L TextGrid :: TGSearch :: Core
 *
 * %% Copyright (C) 2011-2020 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen
 * (http://www.sub.uni-goettingen.de) %%
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L%
 */

package info.textgrid.middleware.tgsearch.api;

import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.portal.Project;
import info.textgrid.namespaces.middleware.tgsearch.portal.ProjectsResponse;

/**
 * Search and utility methods related to project specific portal pages
 *
 * @author Ubbo Veentjer
 *
 */
public interface PortalHelper {

  /**
   * List all project names, descriptions and their configurations.
   * These are set by portalconfig.xml files in every project root
   *
   * @param sid     the sessionID (to include non public config)
   * @param sandbox wether to inlcude sandbox results
   *
   * @return all projectconfigs
   */
  @GET
  @Path("/projects")
  @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
  public ProjectsResponse listProjects(
      @QueryParam("sid") String sid,
      @QueryParam("sandbox") @DefaultValue("false") final boolean sandbox);

  /**
   * Show project name, description, configuration and README.md for one
   * specific project. These are set by the portalconfig.xml and the
   * README.md in the project root
   *
   * @param sid     the sessionID (to include non public config)
   * @param sandbox wether to inlcude sandbox results
   * @param id      the project id
   *
   * @return project config for project with given id
   */
  @GET
  @Path("/project/{id}")
  @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
  public Project projectDetails(
      @QueryParam("sid") String sid,
      @QueryParam("sandbox") @DefaultValue("false") final boolean sandbox,
      @PathParam("id") final String id);

  /**
   * List metadata of toplevel objects for one specific project.
   * Toplevel objects are editions and collections on the root level of a project
   *
   * @param id      the project id
   * @param sid     the sessionID (to include non public results)
   * @param start   result number to start with
   * @param limit   number of entries to return
   * @param sandbox wether to inlcude sandbox results
   *
   * @return metadata of all toplevel objects
   */
  @GET
  @Path("/toplevel/{id}")
  @Produces({ "application/xml", "text/xml" })
  public Response listToplevelAggregations(
      @PathParam("id") String id,
      @QueryParam("sid") String sid,
      @QueryParam("start") @DefaultValue("0") int start,
      @QueryParam("limit") @DefaultValue("10") int limit,
      @QueryParam("sandbox") @DefaultValue("false") final boolean sandbox);

}
