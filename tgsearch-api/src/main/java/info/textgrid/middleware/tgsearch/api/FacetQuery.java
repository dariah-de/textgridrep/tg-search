/*
 * #%L
 * TextGrid :: TGSearch :: API
 * %%
 * Copyright (C) 2011 - 2012 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen (http://www.sub.uni-goettingen.de)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package info.textgrid.middleware.tgsearch.api;

import info.textgrid.namespaces.middleware.tgsearch.FacetResponse;

import java.util.List;

import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;

public interface FacetQuery {

  /**
   * facet textgrid metadata fields
   *
   * @param facetList		metadata fields to create facets
   * @param limit			number of entries per facet, defaults to 10, 0 for all entries
   * @param order			order by count or term ascending (asc) or descending, (desc) e.g.
   * 						setting the string "term:asc". defaults to "count:desc"
   * @param sandbox		show sandboxed (not yet finally published) data
   * @return
   */
  @GET
  @Path("/")
  @Produces({ "application/xml", "text/xml" })
  FacetResponse facetQuery(
      @QueryParam("facet") List<String> facetList,
      @QueryParam("limit") @DefaultValue("10") int limit,
      @QueryParam("order") @DefaultValue("count:desc") String order,
      @QueryParam("sid") String sid,
      @QueryParam("sandbox") @DefaultValue("false") final boolean sandbox);

}
