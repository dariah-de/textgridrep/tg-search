package info.textgrid.middleware.tgsearch.api;

/*This class just contains constants for the target Element in textgridrep URL to define the query mode. 
 * Also sort order definition is here*/

public final class TGSearchConstants {
	
	private TGSearchConstants(){
		//not called		
	}
	
	public static final String FULLTEXT = "structure";
	public static final String METADATA = "metadata";
	public static final String BOTH = "both";
	public static final String DEFAULT_SORT_ORDER = "relevance";
	public static final int KWIC_WIDTH_LIMIT = 40;
	public static final int SEARCH_CLIENT_ITEM_LIMIT = 20;
}
