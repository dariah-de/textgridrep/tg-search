/*
 * #%L
 * TextGrid :: TGSearch :: API
 * %%
 * Copyright (C) 2011 - 2012 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen (http://www.sub.uni-goettingen.de)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package info.textgrid.middleware.tgsearch.api;

import java.util.List;

import info.textgrid.namespaces.middleware.tgsearch.Response;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.DefaultValue;

public interface Search {

	/**
	 * Search for TextGrid objects using the tgsearch query language (tm)
	 *
	 * @param query			the querystring TODO: link usage
	 * @param target		where to do fulltext-searches:
	 * 						one of "structure", "metadata" and "both",
	 * 						defaults to "both"
	 * @param sid			tgauth-sessionid
	 * @param logString
	 * @param sessionid		tgsearch session
	 * @param order			key-value ascending (asc) or descending (desc)
	 * 						and metadata-field like asc:title or desc:author
	 * @param start			result number to start with
	 * @param limit			number of entries to return
	 * @param kwicWidth		number of chars before and after a kwic match
	 * @param wordDistance	max distance beetween two words in fulltext query.
	 * 						ignored if set to a number &lt; 0, then for a hit all words
	 * 						must be contained in one document. defaults to -1
	 * @param path			path of found result(work / edition / aggregations)
	 * 						should be applied to hit
	 * @param allProjects   all Projects should be searched for public data,
	 * 					    warning: this query may be slow, if many results found
	 * @param sandbox		show sandboxed (not yet finally published) data
	 * @param filter		add filter on query results, e.g. for faceting (TODO: Syntax)
	 * @param facetList		metadata fields to create facets
	 * @param facetLimit	number of results to return for each facet
	 *
	 * @return				list of TextGrid objects found
	 */
	@GET
	@Path("/")
	@Produces({ "application/xml", "text/xml" })
	Response getQuery(
			@QueryParam("q") String query,
			@QueryParam("target") @DefaultValue("both") String target,
			@QueryParam("sid") String sid,
			@QueryParam("log") String logString,
			@QueryParam("sessionid") String sessionid,
			@QueryParam("order") @DefaultValue("asc:title") String order,
			@QueryParam("start") @DefaultValue("0") int start,
			@QueryParam("limit") @DefaultValue("20") int limit,
			@QueryParam("kwicWidth") @DefaultValue("40") int  kwicWidth,
			@QueryParam("wordDistance") @DefaultValue("-1") int wordDistance,
			@QueryParam("path") @DefaultValue("false") String path,
			@QueryParam("allProjects") @DefaultValue("false") String allProjects,
			@QueryParam("sandbox") @DefaultValue("false") String sandbox,
			@QueryParam("filter") List<String> filter,
			@QueryParam("facet") List<String> facetList,
			@QueryParam("facetlimit") @DefaultValue("10") int facetLimit
			);


}
