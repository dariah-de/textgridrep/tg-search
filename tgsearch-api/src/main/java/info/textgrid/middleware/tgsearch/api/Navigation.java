/*
 * #%L
 * TextGrid :: TGSearch :: API
 * %%
 * Copyright (C) 2011 - 2012 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen (http://www.sub.uni-goettingen.de)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package info.textgrid.middleware.tgsearch.api;

import info.textgrid.namespaces.middleware.tgsearch.Response;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;

public interface Navigation {

	/**
	 * List objects belonging to a project, filter objects which 
	 * are in an aggregation in same project.
	 * 
	 * @param id
	 * 
	 */
	@GET
	@Path("/{id}")
	@Produces({ "application/xml", "text/xml" })
	Response listProject(
			@PathParam("id") String id,
			@QueryParam("sid") String sid,
			@QueryParam("log") String logstring);

	  /**
	   * List content of aggregation with specific id
	   * 
	   * @param id Identification of a project
	   * @param sid Identification of a session
	   * @param logstring
	   * @return returns a list with Uris achieving the requirements of the sparql query
	   */
	@GET
	@Path("/agg/{id}")
	@Produces({ "application/xml", "text/xml" })
	Response listAggregation(
			@PathParam("id") String id,
			@QueryParam("sid") String sid,
			@QueryParam("log") String logstring);
	
	/**
	 * List all toplevel objects, use for listing public repository
	 * returns empty result on nonpublic instances
	 * 
     * @param sandbox whether to include sandbox in listing, false by default
     * @return returns a list with all Uris in the TopLevel
	 */
	@GET
	@Path("/toplevel")
	@Produces({ "application/xml", "text/xml" })
	Response listToplevelAggregations(
	    @QueryParam("sandbox") @DefaultValue("false") final boolean sandbox);
	
	
	/**
	 * List path to edition and work for a given textgridUri
	 * 
	 * @param id
	 * @param sid
	 * @param logstring
	 * 
	 */
	/*
	@GET
	@Path("/path/{id}")
	@Produces({ "application/xml", "text/xml" })
	public abstract Response getPath(
			@PathParam("id") String id,
			@QueryParam("sid") String sid,
			@QueryParam("log") String logstring);
	*/
    
	/**
	 * List path to edition and work for a given textgridUri
	 * and return path and related metadata
	 * 
	 * @param id
	 * @param sid
	 * @param logstring
	 * 
	 */
	
	/*
	@GET
	@Path("/path/{id}/meta")
	@Produces({ "application/xml", "text/xml" })
	public Response getPathMeta(
			@PathParam("id") String id,
			@QueryParam("sid") String sid,
			@QueryParam("log") String logstring);
	*/

}