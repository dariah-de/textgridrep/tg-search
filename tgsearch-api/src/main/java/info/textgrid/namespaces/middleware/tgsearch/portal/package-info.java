/*
 * #%L
 * TextGrid :: TGSearch :: API
 * %%
 * Copyright (C) 2011 - 2012 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen (http://www.sub.uni-goettingen.de)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
@XmlSchema(namespace = "http://www.textgrid.info/namespaces/middleware/tgsearch",
    xmlns = { 
        @XmlNs(namespaceURI = "http://www.textgrid.info/namespaces/middleware/tgsearch", prefix = "tgs"),
		@XmlNs(namespaceURI = "http://textgrid.info/namespaces/metadata/core/2010", prefix = "")
        //@XmlNs(namespaceURI = "urn:example.com:foo", prefix = "")
    },
    elementFormDefault = jakarta.xml.bind.annotation.XmlNsForm.QUALIFIED)

package info.textgrid.namespaces.middleware.tgsearch.portal;

import jakarta.xml.bind.annotation.XmlNs;
import jakarta.xml.bind.annotation.XmlSchema;

