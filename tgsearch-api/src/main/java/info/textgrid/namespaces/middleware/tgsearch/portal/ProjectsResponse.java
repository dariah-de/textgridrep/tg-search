/*
 * #%L
 * TextGrid :: TGSearch :: API
 * %%
 * Copyright (C) 2011 - 2020 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen (http://www.sub.uni-goettingen.de)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package info.textgrid.namespaces.middleware.tgsearch.portal;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "projects")
public class ProjectsResponse {

  private List<Project> projects = new ArrayList<Project>();

  @XmlElement(name = "project")
  public List<Project> getProjects() {
    return projects;
  }

  public ProjectsResponse setProjects(List<Project> projects) {
    this.projects = projects;
    return this;
  }

  public ProjectsResponse addProject(Project project) {
    this.projects.add(project);
    return this;
  }


}
