/*
 * #%L
 * TextGrid :: TGSearch :: API
 * %%
 * Copyright (C) 2011 - 2020 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen (http://www.sub.uni-goettingen.de)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package info.textgrid.namespaces.middleware.tgsearch.portal;

import jakarta.xml.bind.annotation.XmlRootElement;
import info.textgrid.namespaces.metadata.portalconfig._2020_06_16.Portalconfig;

@XmlRootElement(name = "project")
public class Project {

  private String id;
  private String name;
  private Portalconfig portalconfig;
  private String portalconfigUri;
  private String readme;
  private String readmeUri;
  private long count;

  public String getId() {
    return id;
  }

  public Project setId(String id) {
    this.id = id;
    return this;
  }

  public String getName() {
    return name;
  }

  public Project setName(String name) {
    this.name = name;
    return this;
  }

  public Portalconfig getPortalconfig() {
    return portalconfig;
  }

  public Project setPortalconfig(Portalconfig portalconfig) {
    this.portalconfig = portalconfig;
    return this;
  }

  public String getPortalconfigUri() {
    return portalconfigUri;
  }

  public Project setPortalconfigUri(String portalconfigUri) {
    this.portalconfigUri = portalconfigUri;
    return this;
  }

  public long getCount() {
    return count;
  }

  public Project setCount(long count) {
    this.count = count;
    return this;
  }

  public String getReadme() {
    return readme;
  }

  public Project setReadme(String readme) {
    this.readme = readme;
    return this;
  }

  public String getReadmeUri() {
    return readmeUri;
  }

  public Project setReadmeUri(String readmeUri) {
    this.readmeUri = readmeUri;
    return this;
  }

}
